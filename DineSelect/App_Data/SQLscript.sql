USE [DineSelect]
GO

EXEC sp_rename 'sqlDineSelection.DS_Recipe.RecipeType', 'RecipeTypeID'
GO

USE [DineSelect]
GO

/*** Create Table DS_RecipeType ****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [sqlDineSelection].[DS_RecipeType](
	[RecipeTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_DS_RecipeType] PRIMARY KEY CLUSTERED 
(
	[RecipeTypeID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

/*** INSERT STATEMENTS ***/
INSERT INTO [sqlDineSelection].[DS_RecipeType](Name,Description)
VALUES ('Healthy Cooking','Healthy Cooking')
GO
INSERT INTO [sqlDineSelection].[DS_RecipeType](Name,Description)
VALUES ('Everyday Cooking','Everyday Cooking')
GO
INSERT INTO [sqlDineSelection].[DS_RecipeType](Name,Description)
VALUES ('Cocktails','Cocktails')
GO

/*** UPDATE table Recipe with new IDs ***/
UPDATE [sqlDineSelection].[DS_Recipe] set RecipeTypeID='3' where RecipeTypeID='2'
GO
UPDATE [sqlDineSelection].[DS_Recipe] set RecipeTypeID='2' where RecipeTypeID='1'
GO
UPDATE [sqlDineSelection].[DS_Recipe] set RecipeTypeID='1' where RecipeTypeID='0'
GO

/*** REMOVE 'unknown' from the list ***/
DELETE FROM [DineSelect].[sqlDineSelection].[DS_PaymentMethod]
WHERE [Name]='Unknown'
GO

DELETE FROM [DineSelect].[sqlDineSelection].[DS_PremiumService]
WHERE [Name]='Unknown'
GO

/*DELETE FROM [DineSelect].[sqlDineSelection].[DS_EstablishmentSubType]
WHERE [Name]='Unclassified'
GO*/

/** ADD FOREIGN KEY ***/
ALTER TABLE [sqlDineSelection].[DS_Recipe]
ADD FOREIGN KEY (RecipeTypeID) REFERENCES [sqlDineSelection].[DS_RecipeType](RecipeTypeID)
GO

EXEC sp_rename 'sqlDineSelection.DS_SendJokes.Jock_Content', 'JokeContent'
GO

EXEC sp_rename 'dbo.UserRegistration', 'DS_UserRegistration'
GO

EXEC sp_rename 'sqlDineSelection.DS_Establishment_Reviews', 'DS_EstablishmentReviews'
GO

ALTER TABLE sqlDineSelection.DS_EstablishmentReviews DROP COLUMN Username
GO

ALTER TABLE sqlDineSelection.DS_EstablishmentReviews ADD UserId uniqueidentifier
GO

/****** Object:  Table [sqlDineSelection].[DS_SubwayType]    Script Date: 10/04/2010 11:40:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [sqlDineSelection].[DS_SubwayType](
	[SubwayTypeID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](255) NULL,
 CONSTRAINT [PK_DS_SubwayType] PRIMARY KEY CLUSTERED 
(
	[SubwayTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

INSERT INTO [DineSelect].[sqlDineSelection].[DS_SubwayType]
           ([SubwayTypeID],[Name],[Description])
     VALUES (0,'Name here',null)
GO

INSERT INTO [DineSelect].[sqlDineSelection].[DS_SubwayType]
           ([SubwayTypeID],[Name],[Description])
     VALUES (1,'Name here',null)
GO

INSERT INTO [DineSelect].[sqlDineSelection].[DS_SubwayType]
           ([SubwayTypeID],[Name],[Description])
     VALUES (2,'Name here',null)
GO

INSERT INTO [DineSelect].[sqlDineSelection].[DS_SubwayType]
           ([SubwayTypeID],[Name],[Description])
     VALUES (3,'Name here',null)
GO

INSERT INTO [DineSelect].[sqlDineSelection].[DS_SubwayType]
           ([SubwayTypeID],[Name],[Description])
     VALUES (4,'Name here',null)
GO

INSERT INTO [DineSelect].[sqlDineSelection].[DS_SubwayType]
           ([SubwayTypeID],[Name],[Description])
     VALUES (5,'Name here',null)
GO

INSERT INTO [DineSelect].[sqlDineSelection].[DS_SubwayType]
           ([SubwayTypeID],[Name],[Description])
     VALUES (6,'Name here',null)
GO

INSERT INTO [DineSelect].[sqlDineSelection].[DS_SubwayType]
           ([SubwayTypeID],[Name],[Description])
     VALUES (7,'Name here',null)
GO

INSERT INTO [DineSelect].[sqlDineSelection].[DS_SubwayType]
           ([SubwayTypeID],[Name],[Description])
     VALUES (8,'Name here',null)
GO

INSERT INTO [DineSelect].[sqlDineSelection].[DS_SubwayType]
           ([SubwayTypeID],[Name],[Description])
     VALUES (9,'Name here',null)
GO

/*** CHANGE THE TYPE FROM tinyint to int *****/
ALTER TABLE sqlDineSelection.DS_SubwayStation ALTER COLUMN SubwayType int
GO

/****** Object:  ForeignKey [FK_DS_SubwayStation_DS_SubwayType]    Script Date: 10/04/2010 11:50:16 ******/
ALTER TABLE [sqlDineSelection].[DS_SubwayStation]  WITH CHECK ADD  CONSTRAINT [FK_DS_SubwayStation_DS_SubwayType] FOREIGN KEY([SubwayType])
REFERENCES [sqlDineSelection].[DS_SubwayType] ([SubwayTypeID])
GO
ALTER TABLE [sqlDineSelection].[DS_SubwayStation] CHECK CONSTRAINT [FK_DS_SubwayStation_DS_SubwayType]
GO
