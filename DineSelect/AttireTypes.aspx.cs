﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class AttireTypes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                LinqAttireTypes.Where = Session["whereAttire"] + "";
                LinqAttireTypes.DataBind();
            }
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            DS_AttireType atm = new DS_AttireType();
            atm.Name = txtName.Text;
            atm.Description = txtDescription.Text;
            dine.DS_AttireTypes.InsertOnSubmit(atm);
            dine.SubmitChanges();
            GridView1.DataBind();
            GridView1.DataBind();
            txtDescription.Text = "";
            txtName.Text = "";
            updatePanel2.Update();

            Utilities utilities = new Utilities();
            Panel panel = Page.Master.FindControl("Panel1") as Panel;
            Literal links = panel.FindControl("links") as Literal;
            links.Text = "<ul><li><ul>" + utilities.GetTotalRows() + "</ul></li></ul>";
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                GridView1.AllowPaging = false;
                LinqAttireTypes.DataBind();
                LinqAttireTypes.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel2.Update();
            }
            else
            {
                GridView1.AllowPaging = true;
                GridView1.PageSize = int.Parse(ddlSize.SelectedValue);
                LinqAttireTypes.DataBind();
                LinqAttireTypes.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel2.Update();
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Name"))
            {
                LinqAttireTypes.Where = "Name.Contains(\"" + txtField.Text + "\")";
                LinqAttireTypes.DataBind();
                Session.Add("whereAttire", LinqAttireTypes.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel2.Update();
            }
            else
            {
                LinqAttireTypes.Where = "AttireTypeID == " + txtField.Text;
                LinqAttireTypes.DataBind();
                Session.Add("whereAttire", LinqAttireTypes.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel2.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            LinqAttireTypes.Where = "";
            Session["whereAttire"] = "";
            LinqAttireTypes.DataBind();
            GridView1.DataBind();
            GridView1.DataBind();
            updatePanel2.Update();
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_AttireTypes where a.AttireTypeID == key select a;
            try
            {
                dine.DS_AttireTypes.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqAttireTypes.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel2.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("Panel1") as Panel;
                Literal links = panel.FindControl("links") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRows() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }
    }
}
