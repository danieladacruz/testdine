﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class Cities : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                var countries = from c in dine.DS_Countries
                                orderby c.Name
                                select c;
                ddlCountries.DataSource = countries;
                ddlCountries.DataTextField = "name";
                ddlCountries.DataValueField = "countryID";
                ddlCountries.DataBind();

                if (ddlCountries.Items.Count > 0)
                {
                    BindProvincesForCountry(countries.First().CountryID);
                }
            }
            //else
            //{
            //    if (Session["whereCities"] != null)
            //    {
            //        LinqCities.Where = Session["whereCities"] + "";
            //        LinqCities.DataBind();
            //    }
            //}
        }

        private void BindProvincesForCountry(int country)
        {
            ddlProvinces.Items.Clear();

            DineSelectDataContext dine = new DineSelectDataContext();
            var provinces = from p in dine.DS_Provinces
                            where p.CountryID == country
                            orderby p.Name
                            select p;
            ddlProvinces.DataSource = provinces;
            ddlProvinces.DataTextField = "Name";
            ddlProvinces.DataValueField = "provinceID";
            ddlProvinces.AppendDataBoundItems = true;
            if (provinces.Count() > 1)
            {
                ddlProvinces.Items.Insert(0, new ListItem("", "0"));
            }
            else
            {
                if (provinces.Count() == 1)
                {
                    lblCity.Text = "New City in " + provinces.First().Name + " Province";
                }
            }
            ddlProvinces.DataBind();
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("") ||
                (!ddlCountries.SelectedValue.Equals("0") && !ddlProvinces.SelectedValue.Equals("0")))
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                DS_City city = new DS_City();
                city.Name = txtName.Text;
                city.Description = txtDescription.Text;
                city.ProvinceID = int.Parse(ddlProvinces.SelectedValue);

                dine.DS_Cities.InsertOnSubmit(city);
                dine.SubmitChanges();
                GridView1.DataBind();
                GridView1.DataBind();
                txtName.Text = "";
                txtDescription.Text = "";
                UpdatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
        }

        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0"))
            {
                ddlProvinces.Items.Clear();
                BindProvincesForCountry(int.Parse(ddlCountries.SelectedValue));
            }
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                GridView1.AllowPaging = false;
                LinqCities.DataBind();
                LinqCities.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            else
            {
                GridView1.AllowPaging = true;
                GridView1.PageSize = int.Parse(ddlSize.SelectedValue);
                LinqCities.DataBind();
                LinqCities.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Name"))
            {
                if (!ddlProvinces.SelectedValue.Equals(""))
                {
                    LinqCities.Where = "ProvinceID == " + ddlProvinces.SelectedValue + " && " +
                        "Name.Contains(\"" + txtField.Text + "\")";
                }
                else
                {
                    LinqCities.Where = "Name.Contains(\"" + txtField.Text + "\")";
                }
                LinqCities.DataBind();
                Session.Add("whereCities", LinqCities.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            else
            {
                LinqCities.Where = "CityID == " + txtField.Text;
                LinqCities.DataBind();
                Session.Add("whereCities", LinqCities.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            if (!ddlProvinces.SelectedValue.Equals(""))
            {
                Session["whereCities"] = "ProvinceID == " + ddlProvinces.SelectedValue;
            }
            else
            {
                Session["whereCities"] = "ProvinceID == 0";
            }
            LinqCities.Where = Session["whereCities"]+"";
            LinqCities.DataBind();
            GridView1.DataBind();
            GridView1.DataBind();
            UpdatePanel1.Update();
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_Cities where a.CityID == key select a;
            try
            {
                dine.DS_Cities.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqCities.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }

        protected void ddlProvinces_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCity.Text = "New City in " + ddlProvinces.SelectedItem.Text + " Province";
        }
    }
}
