﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class CityLocalities : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                var countries = from c in dine.DS_Countries
                                orderby c.Name
                                select c;
                ddlCountries.DataSource = countries;
                ddlCountries.DataTextField = "name";
                ddlCountries.DataValueField = "countryID";
                ddlCountries.DataBind();

                if (ddlCountries.Items.Count > 0)
                {
                    BindProvincesForCountry(countries.First().CountryID);
                }
            }
            //else
            //{
            //    if (Session["whereCityLocalities"] != null)
            //    {
            //        LinqLocalities.Where = Session["whereCityLocalities"] + "";
            //        LinqLocalities.DataBind();
            //    }
            //}
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0") &&
                !ddlCities.SelectedValue.Equals("") && !ddlCities.SelectedValue.Equals("0") &&
                !ddlRegions.SelectedValue.Equals("") && !ddlRegions.SelectedValue.Equals("0"))
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                DS_CityLocality locality = new DS_CityLocality();
                locality.CityRegionID = int.Parse(ddlRegions.SelectedValue);
                locality.Description = txtDescription.Text;
                locality.Name = txtName.Text;
                dine.DS_CityLocalities.InsertOnSubmit(locality);
                dine.SubmitChanges();

                txtName.Text = "";
                txtDescription.Text = "";
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
        }

        private void BindProvincesForCountry(int country)
        {
            ddlProvinces.Items.Clear();
            ddlCities.Items.Clear();
            ddlRegions.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var provinces = from p in dine.DS_Provinces
                            where p.CountryID == country
                            select p;
            ddlProvinces.DataSource = provinces;
            ddlProvinces.DataTextField = "Name";
            ddlProvinces.DataValueField = "provinceID";
            ddlProvinces.AppendDataBoundItems = true;
            if (provinces.Count() > 1)
            {
                ddlProvinces.Items.Insert(0, new ListItem("", "0"));
            }
            else
            {
                BindCitiesForProvince(provinces.First().ProvinceID);
            }
            ddlProvinces.DataBind();
        }

        private void BindCitiesForProvince(int id)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var cities = from c in dine.DS_Cities
                         where c.ProvinceID == id
                         select c;

            ddlCities.DataSource = cities;
            ddlCities.DataTextField = "Name";
            ddlCities.DataValueField = "cityID";
            ddlCities.AppendDataBoundItems = true;
            if (cities.Count() > 1)
            {
                ddlCities.Items.Insert(0, new ListItem("", "0"));
            }
            else
            {
                BindRegionsForCity(cities.First().CityID);
            }
            ddlCities.DataBind();
        }

        private void BindRegionsForCity(int id)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var regions = from c in dine.DS_CityRegions
                          where c.CityID == id
                          select c;

            ddlRegions.DataSource = regions;
            ddlRegions.DataTextField = "Name";
            ddlRegions.DataValueField = "CityRegionID";
            ddlRegions.AppendDataBoundItems = true;
            ddlRegions.Items.Insert(0, new ListItem("", "0"));
            ddlRegions.DataBind();

            if (regions.Count() == 1)
            {
                lblLocality.Text = "New Locality in " + regions.First().Name + " Region";
                panelAdd.Enabled = true;
            }
            else
            {
                panelAdd.Enabled = false;
            }
        }

        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0"))
            {
                BindProvincesForCountry(int.Parse(ddlCountries.SelectedValue));
            }
        }



        protected void ddlProvinces_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0"))
            {
                ddlCities.Items.Clear();
                ddlRegions.Items.Clear();
                BindCitiesForProvince(int.Parse(ddlProvinces.SelectedValue));
            }
        }

        protected void ddlCities_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0") &&
                !ddlCities.SelectedValue.Equals("") && !ddlCities.SelectedValue.Equals("0"))
            {
                ddlRegions.Items.Clear();
                BindRegionsForCity(int.Parse(ddlCities.SelectedValue));


            }
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                GridView1.AllowPaging = false;
                LinqLocalities.DataBind();
                LinqLocalities.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            else
            {
                GridView1.AllowPaging = true;
                GridView1.PageSize = int.Parse(ddlSize.SelectedValue);
                LinqLocalities.DataBind();
                LinqLocalities.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Name"))
            {
                if (!ddlRegions.SelectedValue.Equals(""))
                {
                    LinqLocalities.Where = "CityRegionID == " + ddlRegions.SelectedValue + " && " +
                        "Name.Contains(\"" + txtField.Text + "\")";
                }
                else
                {
                    LinqLocalities.Where = "Name.Contains(\"" + txtField.Text + "\")";
                }
                LinqLocalities.DataBind();
                Session.Add("whereCityLocalities", LinqLocalities.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            else
            {
                LinqLocalities.Where = "CityLocalityID == " + txtField.Text;
                LinqLocalities.DataBind();
                Session.Add("whereCityLocalities", LinqLocalities.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            if (!ddlRegions.SelectedValue.Equals(""))
            {
                Session["whereCityLocalities"] = "CityRegionID == " + ddlRegions.SelectedValue;
            }
            else
            {
                Session["whereCityLocalities"] = "CityRegionID == 0";
            }
            LinqLocalities.Where = Session["whereCityLocalities"]+"";
            LinqLocalities.DataBind();
            GridView1.DataBind();
            GridView1.DataBind();
            UpdatePanel1.Update();
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_CityLocalities where a.CityLocalityID == key select a;
            try
            {
                dine.DS_CityLocalities.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqLocalities.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }

        protected void ddlRegions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlRegions.SelectedItem.Text.Equals(""))
            {
                lblLocality.Text = "New Locality in " + ddlRegions.SelectedItem.Text + " Region";
                panelAdd.Enabled = true;
            }
            else
            {
                lblLocality.Text = "New Locality";
                panelAdd.Enabled = false;
            }
        }

    }
}
