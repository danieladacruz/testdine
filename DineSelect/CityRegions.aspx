﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="CityRegions.aspx.cs" Inherits="DineSelect.CityRegions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" language="javascript">
        function Delete(val) {
            document.getElementById("<%= deleteHidden.ClientID %>").value = val;
            document.getElementById('<%=btnOne.ClientID %>').click();
        }

    </script>

    <asp:Button ID="btnOne" runat="server" Text="One" Style="display: none;" OnClick="btnOne_Click" />
    <asp:Label ID="Label1" Width="100%" runat="server" Text="Regions" CssClass="labelTop"></asp:Label>
    <table>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <input type="hidden" id="deleteHidden" runat="server" />
                        <br />
                        <br />
                        <table>
                            <tr>
                                <td>
                                    Rows to Display
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSize" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSize_SelectedIndexChanged">
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                        <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                        <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Search for:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlField" runat="server">
                                        <asp:ListItem Text="Name" Value="Name"></asp:ListItem>
                                        <asp:ListItem Text="ID" Value="ID"></asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtField" runat="server" Width="300px"></asp:TextBox>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="linkSearch" runat="server" OnClick="linkSearch_Click">Search</asp:LinkButton>
                                    &nbsp;
                                    <asp:LinkButton ID="linkClear" runat="server" OnClick="linkClear_Click">Clear</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table width="100%">
                            <tr>
                                <td style="width: 165px">
                                    Please choose a country:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCountries" runat="server" AutoPostBack="True" Style="height: 22px"
                                        AppendDataBoundItems="true" Width="250px" OnSelectedIndexChanged="ddlCountries_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 165px">
                                    Please choose a province:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlProvinces" runat="server" OnSelectedIndexChanged="ddlProvinces_SelectedIndexChanged"
                                        AutoPostBack="True" Style="height: 22px" Width="250px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 165px">
                                    Please choose a city:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCities" runat="server" AutoPostBack="True" Style="height: 22px"
                                        Width="250px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                    <br />
                                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" DataKeyNames="CityRegionID" DataSourceID="LinqRegions"
                                        CssClass="GridViewStyle" HeaderStyle-CssClass="HeaderStyle" AlternatingRowStyle-CssClass="AltRowStyle"
                                        RowStyle-CssClass="RowStyle">
                                        <RowStyle CssClass="RowStyle" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="linkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                                                    <asp:LinkButton ID="DeleteButton" runat="server">Delete</asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="DeleteButton"
                                                        DisplayModalPopupID="ModalPopupExtender1" />
                                                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="DeleteButton"
                                                        PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                                    <asp:Panel ID="PNL" runat="server" Style="display: none; width: 200px; background-color: White;
                                                        border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                                        Are you sure you want to delete this item?
                                                        <br />
                                                        <br />
                                                        <div style="text-align: right;">
                                                            <asp:Button ID="ButtonOk" runat="server" Text="OK" OnClientClick='<%# "Delete(" + Eval("CityRegionID") + ");"%>' />
                                                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="linkUpdate" runat="server" CommandName="Update">Update</asp:LinkButton>
                                                    <asp:LinkButton ID="linkCancel" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CityRegionID" HeaderText="ID" SortExpression="CityRegionID"
                                                InsertVisible="False" ReadOnly="True" />
                                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                                            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            There is no regions for this city.
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                    <asp:LinqDataSource ID="LinqRegions" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                                        EnableDelete="True" EnableInsert="True" EnableUpdate="True" TableName="DS_CityRegions"
                                        Where="CityID == @CityID" OrderBy="Name">
                                        <WhereParameters>
                                            <asp:ControlParameter ControlID="ddlCities" Name="CityID" PropertyName="SelectedValue"
                                                Type="Int32" DefaultValue="0" />
                                        </WhereParameters>
                                    </asp:LinqDataSource>
                                    <br />
                                    <br />
                                    <b>
                                        <asp:Label ID="lblRegion" runat="server" Text="New Region"></asp:Label>
                                    </b>
                                    <asp:Panel ID="panelAdd" runat="server" Enabled="false">
                                        <table class="GridViewStyle">
                                            <tr class="HeaderStyle">
                                                <th align="left">
                                                    Name
                                                </th>
                                                <td>
                                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="add" runat="server"
                                                        ErrorMessage="*" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr class="HeaderStyle">
                                                <th align="left">
                                                    Description
                                                </th>
                                                <td>
                                                    <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="right">
                                                    <asp:LinkButton ID="linkSave" runat="server" OnClick="linkSave_Click" ValidationGroup="add">Save</asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="linkSave"
                                                        DisplayModalPopupID="ModalPopupExtender1" ConfirmOnFormSubmit="true" />
                                                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="linkSave"
                                                        PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                                    <asp:Panel ID="PNL" runat="server" Style="display: none; width: 250px; background-color: White;
                                                        border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                                        Are you sure you want to save this item?
                                                        <br />
                                                        <br />
                                                        <div style="text-align: right;">
                                                            <asp:Button ID="ButtonOk" runat="server" Text="OK" />
                                                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
                    <ProgressTemplate>
                        <img id="Img1" src="images/wait.gif" width="40" height="40" runat="server" alt="Loading" /></ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
</asp:Content>
