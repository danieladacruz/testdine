﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class CityRegions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (!Page.IsPostBack)
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                var countries = from c in dine.DS_Countries
                                orderby c.Name
                                select c;
                ddlCountries.DataSource = countries;
                ddlCountries.DataTextField = "name";
                ddlCountries.DataValueField = "countryID";
                ddlCountries.DataBind();

                if (ddlCountries.Items.Count > 0)
                {
                    BindProvincesForCountry(countries.First().CountryID);
                }
            }
            //else
            //{
            //    if (Session["whereCityRegions"] != null)
            //    {
            //        LinqRegions.Where = Session["whereCityRegions"] + "";
            //        LinqRegions.DataBind();
            //    }
            //}
        }

        private void BindProvincesForCountry(int country)
        {
            ddlProvinces.Items.Clear();
            ddlCities.Items.Clear();

            DineSelectDataContext dine = new DineSelectDataContext();
            var provinces = from p in dine.DS_Provinces
                            where p.CountryID == country
                            select p;
            ddlProvinces.DataSource = provinces;
            ddlProvinces.DataTextField = "Name";
            ddlProvinces.DataValueField = "provinceID";
            ddlProvinces.AppendDataBoundItems = true;
            if (provinces.Count() > 1)
            {
                ddlProvinces.Items.Insert(0, new ListItem("", "0"));
            }
            else
            {
                BindCitiesForProvince(provinces.First().ProvinceID);
            }
            ddlProvinces.DataBind();
        }

        private void BindCitiesForProvince(int id)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var cities = from c in dine.DS_Cities
                         where c.ProvinceID == id
                         select c;

            ddlCities.DataSource = cities;
            ddlCities.DataTextField = "Name";
            ddlCities.DataValueField = "cityID";
            ddlCities.AppendDataBoundItems = true;
            if (cities.Count() > 1)
            {
                ddlCities.Items.Insert(0, new ListItem("", "0"));
            }
            else
            {
                if (cities.Count() == 1)
                {
                    lblRegion.Text = "New Region in " + cities.First().Name + " City";
                }
            }

            if (cities.Count() > 0)
            {
                panelAdd.Enabled = true;
            }
            else
            {
                panelAdd.Enabled = false;
            }
            ddlCities.DataBind();
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            if ((!ddlCountries.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("") && !ddlCities.SelectedValue.Equals("")) ||
                (!ddlCountries.SelectedValue.Equals("0") && !ddlProvinces.SelectedValue.Equals("0") && !ddlCities.SelectedValue.Equals("0")))
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                DS_CityRegion region = new DS_CityRegion();
                region.CityID = int.Parse(ddlCities.SelectedValue);
                region.Description = txtDescription.Text;
                region.Name = txtName.Text;
                dine.DS_CityRegions.InsertOnSubmit(region);
                dine.SubmitChanges();

                txtName.Text = "";
                txtDescription.Text = "";
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
        }

        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0"))
            {
                ddlProvinces.Items.Clear();
                ddlCities.Items.Clear();
                BindProvincesForCountry(int.Parse(ddlCountries.SelectedValue));
            }
        }

        protected void ddlProvinces_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0"))
            {
                ddlCities.Items.Clear();
                BindCitiesForProvince(int.Parse(ddlProvinces.SelectedValue));
            }
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                GridView1.AllowPaging = false;
                LinqRegions.DataBind();
                LinqRegions.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            else
            {
                GridView1.AllowPaging = true;
                GridView1.PageSize = int.Parse(ddlSize.SelectedValue);
                LinqRegions.DataBind();
                LinqRegions.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Name"))
            {
                if (!ddlCities.SelectedValue.Equals(""))
                {
                    Session["whereCityRegions"] = "CityID == " + ddlCities.SelectedValue + " && " +
                        "Name.Contains(\"" + txtField.Text + "\")";
                }
                else
                {
                    Session["whereCityRegions"] = "Name.Contains(\"" + txtField.Text + "\")";
                }
                LinqRegions.Where = Session["whereCityRegions"] + "";
                LinqRegions.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            else
            {
                LinqRegions.Where = "CityRegionID == " + txtField.Text;
                LinqRegions.DataBind();
                Session.Add("whereCityRegions", LinqRegions.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            if (!ddlCities.SelectedValue.Equals(""))
            {
                Session["whereCityRegions"] = "CityID == " + ddlCities.SelectedValue;
            }
            else
            {
                Session["whereCityRegions"] = "CityID == 0";
            }
            LinqRegions.Where = Session["whereCityRegions"] + "";
            LinqRegions.DataBind();
            GridView1.DataBind();
            GridView1.DataBind();
            UpdatePanel1.Update();
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_CityRegions where a.CityRegionID == key select a;
            try
            {
                dine.DS_CityRegions.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqRegions.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }

        protected void ddlCities_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblRegion.Text = "New Region in " + ddlCities.SelectedItem.Text + " City";
        }

    }
}
