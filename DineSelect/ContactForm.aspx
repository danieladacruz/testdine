﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="ContactForm.aspx.cs" Inherits="DineSelect.ContactForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <img id="Img1" alt="Loading" src="images/wait.gif" width="50" height="50" runat="server" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="panelForm" runat="server">
                <table id="rounded-corner">
                    <thead>
                        <tr>
                            <th scope="col" class="rounded-company" style="height: 0px">
                            </th>
                            <th>
                                Enter your data
                            </th>
                            <th scope="col" class="rounded-q4">
                            </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td class="rounded-foot-left" align="center">
                            </td>
                            <td align="center">
                                <asp:LinkButton ID="sendEmail" runat="server" Text="Send Email" OnClick="sendEmail_Click"></asp:LinkButton>
                            </td>
                            <td class="rounded-foot-right">
                            </td>
                        </tr>
                    </tfoot>
                    <tr>
                        <td colspan="3">
                            <table>
                                <tr>
                                    <td>
                                        First name:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="txtFirstName"
                                            Text="*"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Last name:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLastName"
                                            Text="*"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Email:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="txtEmail"
                                            Text="*"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regEmail" ControlToValidate="txtEmail" Text="(Invalid email)"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Your message:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Height="100px" Width="300px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMessage"
                                            Text="*"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>
            <asp:Label ID="lblSuccess" runat="server"></asp:Label>
            <br />
            <asp:HyperLink ID="back" runat="server" Text="Back To Login Page" NavigateUrl="~/Login.aspx"></asp:HyperLink>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


