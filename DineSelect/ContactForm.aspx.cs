﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net.Mail;

namespace DineSelect
{
    public partial class ContactForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string mode = Request.QueryString["mode"];
            if (mode != null)
            {
                if (mode.Equals("ask"))
                {
                    txtMessage.Text = "I would like to have more privileges.\n\nThanks\n";
                }
                else if (mode.Equals("recover"))
                {
                    txtMessage.Text = "I lost my password. Can you resend it to this email?\n\nThanks";
                }
            }
            else
            {
                panelForm.Visible = false;
                lblError.Text = "You didn't arrived here using the correct way!";
            }
        }
        protected void sendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                string mailTo = ConfigurationSettings.AppSettings["mailTo"];
                string smtpServer = ConfigurationSettings.AppSettings["smtpServer"];

                MailMessage mail = new MailMessage();
                mail.To.Add(mailTo);
                mail.From = new MailAddress(txtEmail.Text);
                mail.Subject = Request.QueryString["mode"].Equals("ask") ?
                    "DineSelect: Ask for more privileges" : "DineSelect: Recovery of password";
                mail.Body = "<b>First name:</b> " + txtFirstName + "<br/><b>Last name:</b>" +
                    txtLastName.Text + "<br/><br/><b>Message:</b><br/><br/>" + txtMessage.Text;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient(smtpServer);
                smtp.Send(mail); //send email out
                panelForm.Visible = false;
                lblSuccess.Text = "Your email was sent successfuly. We will back to you soon!";
            }
            catch (Exception ex)
            {
                lblError.Text = "Error: " + ex.Message;
            }
        }
    }
}
