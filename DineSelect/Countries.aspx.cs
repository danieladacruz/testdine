﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class Countries : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                LinqCountries.Where = Session["whereCountries"] + "";
                LinqCountries.DataBind();
            }
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            DS_Country atm = new DS_Country();
            atm.Name = txtName.Text;
            atm.ShortName = txtDescription.Text;
            dine.DS_Countries.InsertOnSubmit(atm);
            dine.SubmitChanges();
            GridView1.DataBind();
            GridView1.DataBind();
            txtDescription.Text = "";
            txtName.Text = "";
            updatePanel1.Update();

            Utilities utilities = new Utilities();
            Panel panel = Page.Master.FindControl("Panel1") as Panel;
            Literal links = panel.FindControl("links") as Literal;
            links.Text = "<ul><li><ul>" + utilities.GetTotalRows() + "</ul></li></ul>";
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                GridView1.AllowPaging = false;
                LinqCountries.DataBind();
                LinqCountries.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
            else
            {
                GridView1.AllowPaging = true;
                GridView1.PageSize = int.Parse(ddlSize.SelectedValue);
                LinqCountries.DataBind();
                LinqCountries.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Name"))
            {
                LinqCountries.Where = "Name.Contains(\"" + txtField.Text + "\")";
                LinqCountries.DataBind();
                Session.Add("whereCountries", LinqCountries.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
            else
            {
                LinqCountries.Where = "CountryID == " + txtField.Text;
                LinqCountries.DataBind();
                Session.Add("whereCountries", LinqCountries.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            LinqCountries.Where = "";
            Session["whereCountries"] = "";
            LinqCountries.DataBind();
            GridView1.DataBind();
            GridView1.DataBind();
            updatePanel1.Update();
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_Countries where a.CountryID == key select a;
            try
            {
                dine.DS_Countries.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqCountries.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("Panel1") as Panel;
                Literal links = panel.FindControl("links") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRows() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }
    }
}
