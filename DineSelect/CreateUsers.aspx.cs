﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.Profile;

namespace DineSelect
{
    public partial class CreateUsers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities utilities = new Utilities();
            if (!utilities.isAdmin(Context.User.Identity.Name))
            {
                Response.Redirect("Default.aspx");
            }
            fillUsers();   
        }

        private void fillUsers()
        {
            List<MembershipUser> users = new List<MembershipUser>();
            gridUsers.DataSource = Membership.GetAllUsers();
            gridUsers.DataBind();
            gridUsers.DataBind();
        }

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow parent = ((LinkButton)sender).Parent.Parent as GridViewRow;
                int index = parent.RowIndex;
                string user = gridUsers.DataKeys[index].Value.ToString();

                Membership.DeleteUser(user);
                string[] roles = Roles.GetRolesForUser(user);
                Roles.RemoveUserFromRoles(user, roles);
                fillUsers();
                fillUsers();
            }
            catch (Exception)
            {
            }
        }

        // Activate event fires when the user hits "next" in the CreateUserWizard
        public void AssignUserToRoles_Activate(object sender, EventArgs e)
        {
            // Databind list of roles in the role manager system to a listbox in the wizard
            AvailableRoles.DataSource = Roles.GetAllRoles(); ;
            AvailableRoles.DataBind();
        }

        // Deactivate event fires when user hits "next" in the CreateUserWizard
        public void AssignUserToRoles_Deactivate(object sender, EventArgs e)
        {

            // Add user to all selected roles from the roles listbox
            for (int i = 0; i < AvailableRoles.Items.Count; i++)
            {
                if (AvailableRoles.Items[i].Selected == true)
                    Roles.AddUserToRole(CreateUserWizard1.UserName, AvailableRoles.Items[i].Value);
            }
        }
    }
}
