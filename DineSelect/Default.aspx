﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DineSelect.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="post">
        <h2 class="title">
            <a href="#">Welcome to DineSelect BackOffice</a></h2>
        <div class="entry">
            <p>
                Here you can Add/Delete/Edit all data present in tables of BackOffice.<br />
                Just click on the name of the table that you want to see at the left menu.</p>
        </div>
    </div>
</asp:Content>


