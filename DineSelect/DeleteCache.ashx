﻿<%@ WebHandler Language="C#" Class="DeleteCache" %>

using System;
using System.IO;
using System.Web;

public class DeleteCache : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        foreach (string cachedThumbnail in 
            Directory.GetFiles(HttpRuntime.CodegenDir, "*.png", SearchOption.AllDirectories)) {
            
            File.Delete(cachedThumbnail);
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write("Cache deleted");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}