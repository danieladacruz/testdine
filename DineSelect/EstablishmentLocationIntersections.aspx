<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="EstablishmentLocationIntersections.aspx.cs" Inherits="DineSelect.EstablishmentLocationIntersections" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" Width="100%" runat="server" Text="Location Intersections"
        CssClass="labelTop"></asp:Label>
    <table>
        <tr>
            <td>
                <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <br />
                        <br />
                        <table width="100%">
                            <tr>
                                <td style="width: 185px">
                                    Please choose an establishment:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlEstablishments" runat="server" AutoPostBack="True" Style="height: 22px"
                                        AppendDataBoundItems="True" Width="350px" DataSourceID="LinqEstablishments" DataTextField="Name"
                                        DataValueField="EstablishmentID" OnSelectedIndexChanged="ddlEstablishments_SelectedIndexChanged">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:LinqDataSource ID="LinqEstablishments" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                                        OrderBy="Name" Select="new (Name, EstablishmentID)" TableName="DS_Establishments"
                                        OnSelecting="LinqEstablishments_Selecting">
                                    </asp:LinqDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top">
                                    <table width="100%" cellpadding="5" cellspacing="5">
                                        <tr>
                                            <td style="width: 50%" valign="top">
                                                <asp:ListBox ID="lstIntersections" runat="server" Rows="10" Width="100%" SelectionMode="Multiple">
                                                </asp:ListBox>
                                            </td>
                                            <td valign="middle">
                                                <asp:LinkButton Text="Add  >>" Width="120px" runat="server" ID="linkAdd" OnClick="linkAdd_Click"></asp:LinkButton>
                                                <br />
                                                <asp:LinkButton Text="<< Remove" Width="120px" runat="server" ID="linkRemove" OnClick="linkRemove_Click"></asp:LinkButton>
                                            </td>
                                            <td valign="top">
                                                <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                                <br />
                                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="EstablishmentID,IntersectionID"
                                                    DataSourceID="LinqEstIntersections" AllowPaging="True" CssClass="GridViewStyle"
                                                    HeaderStyle-CssClass="HeaderStyle" RowStyle-CssClass="RowStyle" SelectedRowStyle-CssClass="SelectedRowStyle"
                                                    AlternatingRowStyle-CssClass="AltRowStyle" OnRowUpdating="GridView_RowUpdating">
                                                    <RowStyle CssClass="RowStyle" />
                                                    <Columns>
                                                        <asp:CommandField ShowEditButton="True" ShowSelectButton="True" />
                                                        <asp:TemplateField HeaderText="Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblN" runat="server" Text='<%# Eval("DS_Intersection.Name") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:Label ID="lblN" runat="server" Text='<%# Eval("DS_Intersection.Name") %>'></asp:Label>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="URL">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblUrl" runat="server" Text='<%# Eval("EstablishmentLocationIntersectionURL") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtURL" runat="server" Text='<%# Eval("EstablishmentLocationIntersectionURL") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Is Nearest">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkNearest" runat="server" Checked='<%# Eval("IsNearest") %>' Enabled="false" />
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:CheckBox ID="chkNearest1" runat="server" Checked='<%# Eval("IsNearest") %>' />
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                    <HeaderStyle CssClass="HeaderStyle" />
                                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    <EmptyDataTemplate>
                                                        No data was returned.</EmptyDataTemplate>
                                                </asp:GridView>
                                                <asp:LinqDataSource ID="LinqEstIntersections" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                                                    EnableDelete="True" EnableInsert="True" EnableUpdate="True" TableName="DS_EstablishmentLocationIntersections"
                                                    Where="EstablishmentID == @EstablishmentID">
                                                    <WhereParameters>
                                                        <asp:ControlParameter ControlID="ddlEstablishments" Name="EstablishmentID" PropertyName="SelectedValue"
                                                            Type="Int32" DefaultValue="0" />
                                                    </WhereParameters>
                                                </asp:LinqDataSource>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
                    <ProgressTemplate>
                        <img id="Img1" src="images/wait.gif" width="40" height="40" runat="server" alt="Loading" /></ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
</asp:Content>
