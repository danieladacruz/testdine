﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace DineSelect
{
    public partial class EstablishmentLocationIntersections : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session.Add("selected", new List<int>());
            }
            lblError.Text = "";
        }

        private void RefreshList()
        {
            if (!ddlEstablishments.SelectedValue.Equals(""))
            {
                int establishmentID = int.Parse(ddlEstablishments.SelectedValue);
                DineSelectDataContext dine = new DineSelectDataContext();

                var intersections = from inters in dine.DS_Intersections
                                    where !(from o in dine.DS_EstablishmentLocationIntersections
                                            where o.EstablishmentID == establishmentID
                                            select o.IntersectionID).Contains(inters.IntersectionID)
                                    orderby inters.Name
                                    select inters;

                lstIntersections.DataSource = intersections;
                lstIntersections.DataTextField = "name";
                lstIntersections.DataValueField = "intersectionID";
                lstIntersections.DataBind();
            }
        }

        protected void ddlEstablishments_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshList();
        }


        protected void linkAdd_Click(object sender, EventArgs e)
        {
            List<int> itemsSelected = new List<int>(); ;

            for (int i = 0; i < lstIntersections.Items.Count; i++)
            {
                if (lstIntersections.Items[i].Selected)
                {
                    itemsSelected.Add(int.Parse(lstIntersections.Items[i].Value));
                }
            }

            /** Refresh List ***/
            RefreshList();

            DineSelectDataContext dine = new DineSelectDataContext();

            foreach (int id in itemsSelected)
            {
                DS_EstablishmentLocationIntersection inter = new DS_EstablishmentLocationIntersection();
                inter.IntersectionID = id;
                inter.EstablishmentID = int.Parse(ddlEstablishments.SelectedValue);
                inter.IsNearest = false;
                inter.EstablishmentLocationIntersectionURL = "";
                dine.DS_EstablishmentLocationIntersections.InsertOnSubmit(inter);
                dine.SubmitChanges();
            }
            GridView1.DataBind();
            GridView1.DataBind();
            RefreshList();
            updatePanel1.Update();

            Utilities utilities = new Utilities();
            Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
            Literal links = panel.FindControl("linksDp") as Literal;
            links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
        }

        protected void GridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var est = from es in dine.DS_EstablishmentLocationIntersections
                      where es.EstablishmentID == int.Parse(ddlEstablishments.SelectedValue)
                      select es;
            List<DS_EstablishmentLocationIntersection> establ = est.ToList();

            bool containsIsNearest = false;
            int est_idmarked = 0, int_idmarked = 0;
            for (int i = 0; i < est.Count() && !containsIsNearest; i++)
            {
                if (establ[i].IsNearest)
                {
                    containsIsNearest = true;
                    est_idmarked = establ[i].EstablishmentID;
                    int_idmarked = establ[i].IntersectionID;
                }
            }
            
            GridViewRow row = GridView1.Rows[e.RowIndex];
            bool check = ((CheckBox)(row.Cells[3].Controls[1])).Checked;
            string url = ((TextBox)(row.Cells[2].Controls[1])).Text;
            int est_id = (int)GridView1.DataKeys[e.RowIndex].Values[0];
                int int_id = (int)GridView1.DataKeys[e.RowIndex].Values[1];

            if (containsIsNearest && check)
            {
                if (est_idmarked == est_id && int_idmarked == int_id)
                {
                    var ests = from es in dine.DS_EstablishmentLocationIntersections
                               where es.EstablishmentID == est_id &&
                               es.IntersectionID == int_id
                               select es;
                    DS_EstablishmentLocationIntersection inter = ests.First();
                    inter.EstablishmentLocationIntersectionURL = url;
                    inter.IsNearest = check;
                    dine.SubmitChanges();

                    LinqEstablishments.DataBind();
                    GridView1.DataBind();
                    GridView1.DataBind();
                }
                else
                {
                    lblError.Text = "Only one intersection can be marked as neares!";
                }
            }
            else
            {
                var ests = from es in dine.DS_EstablishmentLocationIntersections
                          where es.EstablishmentID == est_id &&
                          es.IntersectionID == int_id
                          select es;
                DS_EstablishmentLocationIntersection inter = ests.First();
                inter.EstablishmentLocationIntersectionURL = url;
                inter.IsNearest = check;
                dine.SubmitChanges();

                LinqEstablishments.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
            }
        }

        protected void gridIntersectionsEdit(object sender, GridViewEditEventArgs e)
        {

        }

        protected void linkRemove_Click(object sender, EventArgs e)
        {
            if (GridView1.SelectedDataKey != null)
            {
                int estID = (int)GridView1.SelectedDataKey.Values[0];
                int intID = (int)GridView1.SelectedDataKey.Values[1];

                DineSelectDataContext dine = new DineSelectDataContext();
                var intersections = from inters in dine.DS_EstablishmentLocationIntersections
                                    where inters.IntersectionID == intID &&
                                    inters.EstablishmentID == estID
                                    select inters;
                dine.DS_EstablishmentLocationIntersections.DeleteAllOnSubmit(intersections);
                dine.SubmitChanges();
                GridView1.DataBind();
                GridView1.DataBind();
                GridView1.SelectedIndex = -1;
                RefreshList();
                updatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
        }


        protected void LinqEstablishments_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {

        }
    }
}
