﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class EstablishmentPaymentMethods : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session.Add("available", new List<ListItem>());
                Session.Add("all", new List<ListItem>());
            }
        }

        protected void linkAdd_Click(object sender, EventArgs e)
        {
            List<ListItem> itemsSpecific = (List<ListItem>)Session["available"];
            List<ListItem> itemsAll = (List<ListItem>)Session["all"];
            List<ListItem> indexes = new List<ListItem>();

            for (int i = 0; i < lstAll.Items.Count; i++)
            {
                if (lstAll.Items[i].Selected)
                {
                    itemsSpecific.Add(lstAll.Items[i]);
                    indexes.Add(lstAll.Items[i]);
                }
            }

            foreach (ListItem i in indexes)
            {
                itemsAll.Remove(i);
            }

            lstSpecific.Items.Clear();
            lstSpecific.Items.AddRange(itemsSpecific.ToArray());
            Session["available"] = itemsSpecific;

            lstAll.Items.Clear();
            lstAll.Items.AddRange(itemsAll.ToArray());
            Session["all"] = itemsAll;

            lstAll.ClearSelection();
            lstSpecific.ClearSelection();
        }

        protected void linkRemove_Click(object sender, EventArgs e)
        {
            List<ListItem> itemsSpecific = (List<ListItem>)Session["available"];
            List<ListItem> itemsAll = (List<ListItem>)Session["all"];
            List<ListItem> indexes = new List<ListItem>();

            for (int i = 0; i < lstSpecific.Items.Count; i++)
            {
                if (lstSpecific.Items[i].Selected)
                {
                    itemsAll.Add(lstSpecific.Items[i]);
                    indexes.Add(lstSpecific.Items[i]);
                }
            }

            foreach (ListItem i in indexes)
            {
                itemsSpecific.Remove(i);
            }

            lstSpecific.Items.Clear();
            lstSpecific.Items.AddRange(itemsSpecific.ToArray());
            Session["available"] = itemsSpecific;

            lstAll.Items.Clear();
            lstAll.Items.AddRange(itemsAll.ToArray());
            Session["all"] = itemsAll;

            lstSpecific.ClearSelection();
            lstAll.ClearSelection();
        }

        protected void ddlEstablishments_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlEstablishments.SelectedValue.Equals(""))
            {
                lstSpecific.Items.Clear();
                lstAll.Items.Clear();

                /*** Get the a list of available payments for the currently selected establishment ***/
                List<ListItem> items = new List<ListItem>();
                List<ListItem> itemsAll = new List<ListItem>();
                List<int> paymentIDs = new List<int>();

                DineSelectDataContext dine = new DineSelectDataContext();
                var paymentmethods = from p in dine.DS_EstablishmentPaymentMethods
                                     where p.EstablishmentID == int.Parse(ddlEstablishments.SelectedValue)
                                     orderby p.DS_PaymentMethod.Name
                                     select p.DS_PaymentMethod;
                foreach (DS_PaymentMethod pay in paymentmethods)
                {
                    items.Add(new ListItem(pay.Name, pay.PaymentMethodID + ""));
                    paymentIDs.Add(pay.PaymentMethodID);
                }
                lstSpecific.Items.AddRange(items.ToArray());
                Session["available"] = items;

                /*** all of the available payment methods from DS_PaymentMethod table, 
                 * minus the payment methods that are already in the left lisbox ***/
                var allpayments = from p in dine.DS_PaymentMethods
                                  where !paymentIDs.Contains(p.PaymentMethodID)
                                  orderby p.Name
                                  select p;
                foreach (DS_PaymentMethod pay in allpayments)
                {
                    itemsAll.Add(new ListItem(pay.Name, pay.PaymentMethodID + ""));
                }
                lstAll.Items.AddRange(itemsAll.ToArray());
                Session["all"] = itemsAll;
            }
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            if (!ddlEstablishments.SelectedValue.Equals(""))
            {
                List<ListItem> itemsSpecific = (List<ListItem>)Session["available"];
                List<ListItem> itemsAll = (List<ListItem>)Session["all"];
                int establishmentID = int.Parse(ddlEstablishments.SelectedValue);

                DineSelectDataContext dine = new DineSelectDataContext();
                var paymentmethods = from p in dine.DS_EstablishmentPaymentMethods
                                     where p.EstablishmentID == establishmentID
                                     orderby p.DS_PaymentMethod.Name
                                     select p;

                dine.DS_EstablishmentPaymentMethods.DeleteAllOnSubmit(paymentmethods);

                /*** inserts the new list of payment methods ***/
                foreach (ListItem p in itemsSpecific)
                {
                    DS_EstablishmentPaymentMethod pay = new DS_EstablishmentPaymentMethod();
                    pay.PaymentMethodID = int.Parse(p.Value);
                    pay.EstablishmentID = establishmentID;
                    dine.DS_EstablishmentPaymentMethods.InsertOnSubmit(pay);
                    dine.SubmitChanges();
                }

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
        }
    }
}
