﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class EstablishmentPremiumServices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session.Add("availablePS", new List<ListItem>());
                Session.Add("allPS", new List<ListItem>());
            }
        }

        protected void linkAdd_Click(object sender, EventArgs e)
        {
            List<ListItem> itemsSpecific = (List<ListItem>)Session["availablePS"];
            List<ListItem> itemsAll = (List<ListItem>)Session["allPS"];
            List<ListItem> indexes = new List<ListItem>();

            for (int i = 0; i < lstAll.Items.Count; i++)
            {
                if (lstAll.Items[i].Selected)
                {
                    itemsSpecific.Add(lstAll.Items[i]);
                    indexes.Add(lstAll.Items[i]);
                }
            }

            foreach (ListItem i in indexes)
            {
                itemsAll.Remove(i);
            }

            lstSpecific.Items.Clear();
            lstSpecific.Items.AddRange(itemsSpecific.ToArray());
            Session["availablePS"] = itemsSpecific;

            lstAll.Items.Clear();
            lstAll.Items.AddRange(itemsAll.ToArray());
            Session["allPS"] = itemsAll;

            lstSpecific.ClearSelection();
            lstAll.ClearSelection();
        }

        protected void linkRemove_Click(object sender, EventArgs e)
        {
            List<ListItem> itemsSpecific = (List<ListItem>)Session["availablePS"];
            List<ListItem> itemsAll = (List<ListItem>)Session["allPS"];
            List<ListItem> indexes = new List<ListItem>();

            for (int i = 0; i < lstSpecific.Items.Count; i++)
            {
                if (lstSpecific.Items[i].Selected)
                {
                    itemsAll.Add(lstSpecific.Items[i]);
                    indexes.Add(lstSpecific.Items[i]);
                }
            }

            foreach (ListItem i in indexes)
            {
                itemsSpecific.Remove(i);
            }

            lstSpecific.Items.Clear();
            lstSpecific.Items.AddRange(itemsSpecific.ToArray());
            Session["availablePS"] = itemsSpecific;

            lstAll.Items.Clear();
            lstAll.Items.AddRange(itemsAll.ToArray());
            Session["allPS"] = itemsAll;

            lstSpecific.ClearSelection();
            lstAll.ClearSelection();
        }

        protected void ddlEstablishments_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlEstablishments.SelectedValue.Equals(""))
            {
                lstSpecific.Items.Clear();
                lstAll.Items.Clear();

                /*** Get the a list of available payments for the currently selected establishment ***/
                List<ListItem> items = new List<ListItem>();
                List<ListItem> itemsAll = new List<ListItem>();
                List<int> paymentIDs = new List<int>();

                DineSelectDataContext dine = new DineSelectDataContext();
                var premiumServices = from p in dine.DS_EstablishmentPremiumServices
                                     where p.EstablishmentID == int.Parse(ddlEstablishments.SelectedValue)
                                     orderby p.DS_PremiumService.Name
                                     select p.DS_PremiumService;
                foreach (DS_PremiumService pay in premiumServices)
                {
                    items.Add(new ListItem(pay.Name, pay.PremiumServiceID + ""));
                    paymentIDs.Add(pay.PremiumServiceID);
                }
                lstSpecific.Items.AddRange(items.ToArray());
                Session["availablePS"] = items;

                /*** all of the available payment methods from DS_PaymentMethod table, 
                 * minus the payment methods that are already in the left lisbox ***/
                var allpayments = from p in dine.DS_PremiumServices
                                  where !paymentIDs.Contains(p.PremiumServiceID)
                                  orderby p.Name
                                  select p;
                foreach (DS_PremiumService pay in allpayments)
                {
                    itemsAll.Add(new ListItem(pay.Name, pay.PremiumServiceID + ""));
                }
                lstAll.Items.AddRange(itemsAll.ToArray());
                Session["allPS"] = itemsAll;
            }
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            if (!ddlEstablishments.SelectedValue.Equals(""))
            {
                List<ListItem> itemsSpecific = (List<ListItem>)Session["availablePS"];
                List<ListItem> itemsAll = (List<ListItem>)Session["allPS"];
                int establishmentID = int.Parse(ddlEstablishments.SelectedValue);

                DineSelectDataContext dine = new DineSelectDataContext();
                var paymentmethods = from p in dine.DS_EstablishmentPremiumServices
                                     where p.EstablishmentID == establishmentID
                                     orderby p.DS_PremiumService.Name
                                     select p;

                dine.DS_EstablishmentPremiumServices.DeleteAllOnSubmit(paymentmethods);

                /*** inserts the new list of payment methods ***/
                foreach (ListItem p in itemsSpecific)
                {
                    DS_EstablishmentPremiumService pay = new DS_EstablishmentPremiumService();
                    pay.PremiumServiceID = int.Parse(p.Value);
                    pay.EstablishmentID = establishmentID;
                    dine.DS_EstablishmentPremiumServices.InsertOnSubmit(pay);
                    dine.SubmitChanges();
                }

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
        }
    }
}
