﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="EstablishmentReviews.aspx.cs" Inherits="DineSelect.EstablishmentReviews" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        function onOkEdit() {
            document.getElementById('<%=editReview.ClientID%>').click();
        }

        function edit(item) {
            document.getElementById('<%=hiddenValue.ClientID%>').value = item;
        }

        function hideModalPopupEdit() {
            var popup = $find('modalPopupEdit');
            popup.hide();
        }

        function Delete(val) {
            document.getElementById("<%= deleteHidden.ClientID %>").value = val;
            document.getElementById('<%=btnOne.ClientID %>').click();
        }

    </script>

    <asp:Button ID="btnOne" runat="server" Text="One" Style="display: none;" OnClick="btnOne_Click" />
    <asp:Label ID="Label1" Width="100%" runat="server" Text="Establishments Reviews" CssClass="labelTop"></asp:Label>
    <asp:Button ID="editReview" runat="server" BackColor="White" BorderStyle="None" Width="0px"
        Height="1px" OnClick="editReview_Click" UseSubmitBehavior="false" />
    <input type="hidden" runat="server" id="hiddenValue" />
    <table>
        <tr>
            <td>
                <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <input type="hidden" id="deleteHidden" runat="server" />
                        <table width="100%">
                            <tr>
                                <td style="width: 195px">
                                    Please choose an Establishment:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlEstablishment" runat="server" AutoPostBack="True" Style="height: 22px"
                                        AppendDataBoundItems="true" Width="250px" OnSelectedIndexChanged="ddlEstablishment_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 195px">
                                    Please choose a User:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlUsers" runat="server" AutoPostBack="True" Style="height: 22px"
                                        Width="250px" OnSelectedIndexChanged="ddlUsers_SelectedIndexChanged" AppendDataBoundItems="true">
                                    </asp:DropDownList>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                    <br />
                                    <asp:ListView ID="ListView1" runat="server" DataKeyNames="ReviewID" DataSourceID="LinqReviews">
                                        <ItemTemplate>
                                            <tr class="RowStyle">
                                                <td>
                                                    <asp:LinkButton ID="linkEditContent" OnClientClick='<%# "edit(" + Eval("ReviewID") + ")" %>'
                                                        runat="server" CommandName="Select" CausesValidation="false" OnClick="edit_Click">Edit</asp:LinkButton>
                                                    <asp:LinkButton ID="DeleteButton" runat="server">Delete</asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="DeleteButton"
                                                        DisplayModalPopupID="ModalPopupExtender1" />
                                                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="DeleteButton"
                                                        PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                                    <asp:Panel ID="PNL" runat="server" Style="display: none; width: 200px; background-color: White;
                                                        border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                                        Are you sure you want to delete this item?
                                                        <br />
                                                        <br />
                                                        <div style="text-align: right;">
                                                            <asp:Button ID="ButtonOk" runat="server" Text="OK" OnClientClick='<%# "Delete(" + Eval("ReviewID") + ");"%>' />
                                                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                                <td>
                                                    <asp:DynamicControl runat="server" DataField="ReviewDtt" Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl ID="DynamicControl3" runat="server" DataField="DS_Establishment"
                                                        Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl ID="DynamicControl4" runat="server" DataField="DS_UserRegistration"
                                                        Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl runat="server" DataField="ReviewSubject" Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl runat="server" DataField="ReviewText" Mode="ReadOnly" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="AltRowStyle">
                                                <td>
                                                    <asp:LinkButton ID="linkEditContent" OnClientClick='<%# "edit(" + Eval("ReviewID") + ")" %>'
                                                        runat="server" CommandName="Select" CausesValidation="false" OnClick="edit_Click">Edit</asp:LinkButton>
                                                    <asp:LinkButton ID="DeleteButton" runat="server">Delete</asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="DeleteButton"
                                                        DisplayModalPopupID="ModalPopupExtender1" />
                                                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="DeleteButton"
                                                        PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                                    <asp:Panel ID="PNL" runat="server" Style="display: none; width: 200px; background-color: White;
                                                        border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                                        Are you sure you want to delete this item?
                                                        <br />
                                                        <br />
                                                        <div style="text-align: right;">
                                                            <asp:Button ID="ButtonOk" runat="server" Text="OK" OnClientClick='<%# "Delete(" + Eval("ReviewID") + ");"%>' />
                                                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                                <td>
                                                    <asp:DynamicControl runat="server" DataField="ReviewDtt" Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl ID="DynamicControl1" runat="server" DataField="DS_Establishment"
                                                        Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl ID="DynamicControl2" runat="server" DataField="DS_UserRegistration"
                                                        Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl runat="server" DataField="ReviewSubject" Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl runat="server" DataField="ReviewText" Mode="ReadOnly" />
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                        <EmptyDataTemplate>
                                            <table runat="server" class="GridViewStyle">
                                                <tr>
                                                    <td>
                                                        No data was returned.
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                        <LayoutTemplate>
                                            <table runat="server" class="GridViewStyle">
                                                <tr runat="server">
                                                    <td runat="server">
                                                        <table id="itemPlaceholderContainer" runat="server" border="0" style="">
                                                            <tr runat="server" class="HeaderStyle">
                                                                <th runat="server">
                                                                </th>
                                                                <th runat="server">
                                                                    ReviewDtt
                                                                </th>
                                                                <th id="Th1" runat="server">
                                                                    Establishment
                                                                </th>
                                                                <th id="Th2" runat="server">
                                                                    User
                                                                </th>
                                                                <th runat="server">
                                                                    Subject
                                                                </th>
                                                                <th runat="server">
                                                                    Text
                                                                </th>
                                                            </tr>
                                                            <tr id="itemPlaceholder" runat="server">
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr runat="server">
                                                    <td runat="server" style="">
                                                        <asp:DataPager ID="DataPager1" runat="server">
                                                            <Fields>
                                                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowNextPageButton="False"
                                                                    ShowPreviousPageButton="False" />
                                                                <asp:NumericPagerField />
                                                                <asp:NextPreviousPagerField ButtonType="Button" ShowLastPageButton="True" ShowNextPageButton="False"
                                                                    ShowPreviousPageButton="False" />
                                                            </Fields>
                                                        </asp:DataPager>
                                                    </td>
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                    </asp:ListView>
                                    <asp:LinqDataSource ID="LinqReviews" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                                        EnableDelete="True" EnableInsert="True" EnableUpdate="True" TableName="DS_EstablishmentReviews"
                                        Where="EstablishmentID == @EstablishmentID &amp;&amp; UserId == @UserId">
                                        <WhereParameters>
                                            <asp:ControlParameter ControlID="ddlEstablishment" Name="EstablishmentID" PropertyName="SelectedValue"
                                                Type="Int32" DefaultValue="0" />
                                        </WhereParameters>
                                    </asp:LinqDataSource>
                                    <br />
                                    <br />
                                    <b>New Review</b>
                                    <table class="GridViewStyle">
                                        <tr>
                                            <th align="left" class="HeaderStyle">
                                                Establishment
                                            </th>
                                            <td style="background-color: White">
                                                <asp:DropDownList ID="ddlEstablishmentsToAdd" runat="server" ValidationGroup="add">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                    ControlToValidate="ddlEstablishmentsToAdd" ValidationGroup="add"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th align="left" class="HeaderStyle">
                                                User
                                            </th>
                                            <td>
                                                <asp:DropDownList ID="ddlUsersToAdd" runat="server" AppendDataBoundItems="true" ValidationGroup="add">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                    ControlToValidate="ddlUsersToAdd" ValidationGroup="add"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th align="left" class="HeaderStyle">
                                                Subject
                                            </th>
                                            <td>
                                                <asp:TextBox Width="200px" ID="txtSubjectToAdd" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th align="left" class="HeaderStyle">
                                                Text
                                            </th>
                                            <td>
                                                <asp:TextBox Width="200px" ID="txtTextToAdd" TextMode="MultiLine" Height="100px"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right">
                                                <asp:LinkButton ID="linkSave" runat="server" OnClick="linkSave_Click" ValidationGroup="add">Save</asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="linkSave"
                                                    DisplayModalPopupID="ModalPopupExtender1" ConfirmOnFormSubmit="true" />
                                                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="linkSave"
                                                    PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                                <asp:Panel ID="PNL" runat="server" Style="display: none; width: 250px; background-color: White;
                                                    border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                                    Are you sure you want to save this item?
                                                    <br />
                                                    <br />
                                                    <div style="text-align: right;">
                                                        <asp:Button ID="ButtonOk" runat="server" Text="OK" />
                                                        <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                                    </div>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <asp:Panel ID="PanelEditContent" runat="server" Style="display: none" CssClass="modalPopup"
                            Width="400px">
                            <asp:Panel ID="PanelEditContentDrag" runat="server" Style="cursor: move; background-color: #DDDDDD;
                                border: solid 1px Gray; color: Black">
                                <div>
                                    <p>
                                        Edit the review:</p>
                                </div>
                            </asp:Panel>
                            <div>
                                <br />
                                <br />
                                <center>
                                    <table class="GridViewStyle">
                                        <tr>
                                            <th align="left" class="HeaderStyle">
                                                Establishment
                                            </th>
                                            <td style="background-color: White">
                                                <asp:Label ID="lblEstablishment" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th align="left" class="HeaderStyle">
                                                Review Date/Time
                                            </th>
                                            <td style="background-color: White">
                                                <asp:Label ID="lblReviewDtt" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th align="left" class="HeaderStyle">
                                                User
                                            </th>
                                            <td>
                                                <asp:DropDownList ID="ddlUsersToEdit" runat="server" AppendDataBoundItems="true">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                    ControlToValidate="ddlUsersToEdit" ValidationGroup="edit"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th align="left" class="HeaderStyle">
                                                Subject
                                            </th>
                                            <td>
                                                <asp:TextBox Width="200px" ID="txtSubject" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th align="left" class="HeaderStyle">
                                                Text
                                            </th>
                                            <td>
                                                <asp:TextBox Width="200px" ID="txtText" TextMode="MultiLine" Height="100px" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <center>
                                        <p style="text-align: center;">
                                            <asp:Button ID="OkEditButton" runat="server" Text="Save" CausesValidation="false" />
                                            <asp:Button ID="cancelEditButton" runat="server" Text="Cancel" CausesValidation="false" />
                                        </p>
                                    </center>
                                </center>
                            </div>
                        </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="modalPopupEdit" runat="server" TargetControlID="PanelEditContent"
                            PopupControlID="PanelEditContent" BackgroundCssClass="modalBackground" OkControlID="OkEditButton"
                            DropShadow="true" OnOkScript="onOkEdit()" OnCancelScript="hideModalPopupEdit()"
                            CancelControlID="cancelEditButton" PopupDragHandleControlID="PanelEditContentDrag"
                            BehaviorID="modalPopupEdit" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="update">
                    <ProgressTemplate>
                        <img id="Img1" src="images/wait.gif" width="40" height="40" runat="server" alt="Loading" /></ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
</asp:Content>
