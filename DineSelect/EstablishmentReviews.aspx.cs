﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace DineSelect
{
    public partial class EstablishmentReviews : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                var establs = from c in dine.DS_Establishments
                              orderby c.Name
                              select c;
                ddlEstablishment.DataSource = establs;
                ddlEstablishment.DataTextField = "Name";
                ddlEstablishment.DataValueField = "EstablishmentID";
                if (establs.Count() > 1)
                {
                    ddlEstablishment.Items.Insert(0, new ListItem("", ""));
                }
                else if (establs.Count() == 1)
                {
                    BindUsersForEstablishment(establs.First().EstablishmentID);
                }
                ddlEstablishment.DataBind();

                BindAllUsers(ddlUsersToAdd);
                BindAllEstablishments(ddlEstablishmentsToAdd);
            }
        }

        protected void ddlEstablishment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlEstablishment.SelectedValue.Equals(""))
            {
                BindUsersForEstablishment(int.Parse(ddlEstablishment.SelectedValue));
            }
        }

        private void BindAllEstablishments(DropDownList ddl)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var establs = from c in dine.DS_Establishments
                          orderby c.Name
                          select c;
            ddl.DataSource = establs;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "EstablishmentID";
            if (establs.Count() > 1)
            {
                ddl.Items.Insert(0, new ListItem("", ""));
            }
            ddl.DataBind();
        }

        private void BindUsersForEstablishment(int estID)
        {
            ddlUsers.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var users = from c in dine.DS_EstablishmentReviews
                        orderby c.DS_UserRegistration.FirstName, c.DS_UserRegistration.LastName
                        where c.EstablishmentID == estID
                        select new { name = c.DS_UserRegistration.FirstName + " " + c.DS_UserRegistration.LastName, userid = c.UserId };
            ddlUsers.DataSource = users;
            ddlUsers.DataTextField = "name";
            ddlUsers.DataValueField = "userid";
            if (users.Count() > 1)
            {
                ddlUsers.Items.Insert(0, new ListItem("", ""));
            }
            ddlUsers.DataBind();
        }

        private void BindAllUsers(DropDownList ddl)
        {
            ddl.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var users = from c in dine.DS_UserRegistrations
                        orderby c.FirstName, c.LastName
                        select new { name = c.FirstName + " " + c.LastName, userid = c.UserId };
            ddl.DataSource = users;
            ddl.DataTextField = "name";
            ddl.DataValueField = "userid";
            if (users.Count() > 1)
            {
                ddl.Items.Insert(0, new ListItem("", ""));
            }
            ddl.DataBind();
        }

        protected void edit_Click(object sender, EventArgs e)
        {
            string reviewIDStr = hiddenValue.Value;
            if (!reviewIDStr.Equals(""))
            {
                int reviewID = int.Parse(reviewIDStr);
                DineSelectDataContext dine = new DineSelectDataContext();
                var reviews = from j in dine.DS_EstablishmentReviews where j.ReviewID == reviewID select j;
                DS_EstablishmentReview review = reviews.First();

                BindAllUsers(ddlUsersToEdit);

                lblEstablishment.Text = review.DS_Establishment.Name;
                lblReviewDtt.Text = review.ReviewDtt.HasValue ? review.ReviewDtt.Value.ToString() : "";
                txtSubject.Text = review.ReviewSubject;
                txtText.Text = review.ReviewText;
                if (review.UserId.HasValue)
                {
                    ddlUsersToEdit.SelectedValue = review.UserId.Value.ToString();
                }
            }
            modalPopupEdit.Show();
        }

        protected void editReview_Click(object sender, EventArgs e)
        {
            string reviewIDStr = hiddenValue.Value;
            if (!reviewIDStr.Equals(""))
            {
                int reviewID = int.Parse(reviewIDStr);
                DineSelectDataContext dine = new DineSelectDataContext();
                var reviews = from j in dine.DS_EstablishmentReviews where j.ReviewID == reviewID select j;
                DS_EstablishmentReview review = reviews.First();

                review.ReviewSubject = txtSubject.Text;
                review.ReviewText = txtText.Text;
                if (!ddlUsersToEdit.SelectedValue.Equals(""))
                {
                    review.UserId = new Guid(ddlUsersToEdit.SelectedValue);
                }
                dine.SubmitChanges();
                ListView1.DataBind();
            }
        }

        protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlUsers.SelectedValue.Equals(""))
            {
                if (LinqReviews.WhereParameters.Count == 1)
                {
                    LinqReviews.WhereParameters.Add("UserId", DbType.Guid, ddlUsers.SelectedValue);
                    LinqReviews.DataBind();
                }
                else
                {
                    LinqReviews.WhereParameters["UserId"].DefaultValue = ddlUsers.SelectedValue;
                }
                //update.Update();
            }
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            DS_EstablishmentReview est = new DS_EstablishmentReview();
            est.EstablishmentID = int.Parse(ddlEstablishmentsToAdd.SelectedValue);
            est.UserId = new Guid(ddlUsersToAdd.SelectedValue);
            est.ReviewDtt = DateTime.Now;
            est.ReviewSubject = txtSubjectToAdd.Text;
            est.ReviewText = txtTextToAdd.Text;
            dine.DS_EstablishmentReviews.InsertOnSubmit(est);
            dine.SubmitChanges();
            ListView1.DataBind();
            ListView1.DataBind();
            txtTextToAdd.Text = "";
            txtSubjectToAdd.Text = "";
            ddlUsersToAdd.SelectedValue = "";
            ddlEstablishmentsToAdd.SelectedValue = "";
            update.Update();

            Utilities utilities = new Utilities();
            Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
            Literal links = panel.FindControl("linksDp") as Literal;
            links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_EstablishmentReviews where a.ReviewID == key select a;
            try
            {
                dine.DS_EstablishmentReviews.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqReviews.DataBind();
                ListView1.DataBind();
                ListView1.DataBind();
                update.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }
    }

}
