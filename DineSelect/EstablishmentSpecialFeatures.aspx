﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="EstablishmentSpecialFeatures.aspx.cs" Inherits="DineSelect.EstablishmentSpecialFeatures" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" Width="100%" runat="server" Text="Establishment Special Features"
        CssClass="labelTop"></asp:Label>
    <br />
    <table width="100%">
        <tr>
            <td style="width: 90%">
                <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table width="100%">
                            <tr>
                                <td style="width: 165px">
                                    Please choose an establishment:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlEstablishments" runat="server" AutoPostBack="True" Style="height: 22px"
                                        AppendDataBoundItems="True" Width="350px" DataSourceID="LinqEstablishments" DataTextField="Name"
                                        DataValueField="EstablishmentID" OnSelectedIndexChanged="ddlEstablishments_SelectedIndexChanged">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:LinqDataSource ID="LinqEstablishments" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                                        OrderBy="Name" Select="new (Name, EstablishmentID)" TableName="DS_Establishments">
                                    </asp:LinqDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table width="100%" cellpadding="5" cellspacing="5">
                                        <tr>
                                            <td>
                                                All Special Features
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                Available Special Features in this Establishment
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 38%" valign="top">
                                                <asp:ListBox ID="lstAll" runat="server" Rows="10" Width="100%" SelectionMode="Multiple">
                                                </asp:ListBox>
                                            </td>
                                            <td align="center">
                                                <asp:Button Text="Add >>" runat="server" ID="linkAdd" OnClick="linkAdd_Click" Width="80px">
                                                </asp:Button>
                                                <br />
                                                <asp:Button Text="<< Remove" runat="server" ID="linkRemove" OnClick="linkRemove_Click" Width="80px">
                                                </asp:Button>
                                            </td>
                                            <td style="width: 38%" valign="top">
                                                <asp:ListBox ID="lstSpecific" runat="server" Rows="10" Width="100%" SelectionMode="Multiple">
                                                </asp:ListBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="right">
                                                <asp:LinkButton Text="Save" runat="server" ID="linkSave" OnClick="linkSave_Click"></asp:LinkButton>
                                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="linkSave"
                                                    DisplayModalPopupID="ModalPopupExtender1" ConfirmOnFormSubmit="true" />
                                                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="linkSave"
                                                    PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                                <asp:Panel ID="PNL" runat="server" Style="display: none; width: 250px; background-color: White;
                                                    border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                                    Are you sure you want to save the changes?
                                                    <br />
                                                    <br />
                                                    <div style="text-align: right;">
                                                        <asp:Button ID="ButtonOk" runat="server" Text="OK" />
                                                        <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                                    </div>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
                    <ProgressTemplate>
                        <img id="Img1" src="images/wait.gif" width="40" height="40" runat="server" alt="Loading" /></ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
</asp:Content>
