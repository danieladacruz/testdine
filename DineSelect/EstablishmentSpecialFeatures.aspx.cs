﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class EstablishmentSpecialFeatures : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session.Add("availableSF", new List<ListItem>());
                Session.Add("allSF", new List<ListItem>());
            }
        }

        protected void linkAdd_Click(object sender, EventArgs e)
        {
            List<ListItem> itemsSpecific = (List<ListItem>)Session["availableSF"];
            List<ListItem> itemsAll = (List<ListItem>)Session["allSF"];
            List<ListItem> indexes = new List<ListItem>();

            for (int i = 0; i < lstAll.Items.Count; i++)
            {
                if (lstAll.Items[i].Selected)
                {
                    itemsSpecific.Add(lstAll.Items[i]);
                    indexes.Add(lstAll.Items[i]);
                }
            }

            foreach (ListItem i in indexes)
            {
                itemsAll.Remove(i);
            }

            lstSpecific.Items.Clear();
            lstSpecific.Items.AddRange(itemsSpecific.ToArray());
            Session["availableSF"] = itemsSpecific;

            lstAll.Items.Clear();
            lstAll.Items.AddRange(itemsAll.ToArray());
            Session["allSF"] = itemsAll;

            lstSpecific.ClearSelection();
            lstAll.ClearSelection();
        }

        protected void linkRemove_Click(object sender, EventArgs e)
        {
            List<ListItem> itemsSpecific = (List<ListItem>)Session["availableSF"];
            List<ListItem> itemsAll = (List<ListItem>)Session["allSF"];
            List<ListItem> indexes = new List<ListItem>();

            for (int i = 0; i < lstSpecific.Items.Count; i++)
            {
                if (lstSpecific.Items[i].Selected)
                {
                    itemsAll.Add(lstSpecific.Items[i]);
                    indexes.Add(lstSpecific.Items[i]);
                }
            }

            foreach (ListItem i in indexes)
            {
                itemsSpecific.Remove(i);
            }

            lstSpecific.Items.Clear();
            lstSpecific.Items.AddRange(itemsSpecific.ToArray());
            Session["availableSF"] = itemsSpecific;

            lstAll.Items.Clear();
            lstAll.Items.AddRange(itemsAll.ToArray());
            Session["allSF"] = itemsAll;

            lstSpecific.ClearSelection();
            lstAll.ClearSelection();
        }

        protected void ddlEstablishments_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlEstablishments.SelectedValue.Equals(""))
            {
                lstSpecific.Items.Clear();
                lstAll.Items.Clear();

                /*** Get the a list of available payments for the currently selected establishment ***/
                List<ListItem> items = new List<ListItem>();
                List<ListItem> itemsAll = new List<ListItem>();
                List<int> paymentIDs = new List<int>();

                DineSelectDataContext dine = new DineSelectDataContext();
                var premiumServices = from p in dine.DS_EstablishmentSpecialFeatures
                                      where p.EstablishmentID == int.Parse(ddlEstablishments.SelectedValue)
                                      orderby p.DS_SpecialFeature.Name
                                      select p.DS_SpecialFeature;
                foreach (DS_SpecialFeature pay in premiumServices)
                {
                    items.Add(new ListItem(pay.Name, pay.SpecialFeatureID + ""));
                    paymentIDs.Add(pay.SpecialFeatureID);
                }
                lstSpecific.Items.AddRange(items.ToArray());
                Session["availableSF"] = items;

                /*** all of the available payment methods from DS_PaymentMethod table, 
                 * minus the payment methods that are already in the left lisbox ***/
                var allpayments = from p in dine.DS_SpecialFeatures
                                  where !paymentIDs.Contains(p.SpecialFeatureID)
                                  orderby p.Name
                                  select p;
                foreach (DS_SpecialFeature pay in allpayments)
                {
                    itemsAll.Add(new ListItem(pay.Name, pay.SpecialFeatureID + ""));
                }
                lstAll.Items.AddRange(itemsAll.ToArray());
                Session["allSF"] = itemsAll;
            }
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            if (!ddlEstablishments.SelectedValue.Equals(""))
            {
                List<ListItem> itemsSpecific = (List<ListItem>)Session["availableSF"];
                List<ListItem> itemsAll = (List<ListItem>)Session["allSF"];
                int establishmentID = int.Parse(ddlEstablishments.SelectedValue);

                DineSelectDataContext dine = new DineSelectDataContext();
                var paymentmethods = from p in dine.DS_EstablishmentSpecialFeatures
                                     where p.EstablishmentID == establishmentID
                                     orderby p.DS_SpecialFeature.Name
                                     select p;

                dine.DS_EstablishmentSpecialFeatures.DeleteAllOnSubmit(paymentmethods);

                /*** inserts the new list of payment methods ***/
                foreach (ListItem p in itemsSpecific)
                {
                    DS_EstablishmentSpecialFeature pay = new DS_EstablishmentSpecialFeature();
                    pay.SpecialFeatureID = int.Parse(p.Value);
                    pay.EstablishmentID = establishmentID;
                    dine.DS_EstablishmentSpecialFeatures.InsertOnSubmit(pay);
                    dine.SubmitChanges();
                }

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
        }
    }
}
