﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class EstablishmentSubType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void ddlEstablishments_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlEstablishmentTypes.SelectedValue.Equals(""))
            {
                panelAdd.Enabled = true;
            }
            else
            {
                panelAdd.Enabled = false;
            }
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            DS_EstablishmentSubType st = new DS_EstablishmentSubType();
            st.Description = txtDescription.Text;
            st.EstablishmentSubTypeURL = txtURL.Text;
            st.EstablishmentTypeID = int.Parse(ddlEstablishmentTypes.SelectedValue);
            st.Name = txtName.Text;
            dine.DS_EstablishmentSubTypes.InsertOnSubmit(st);
            dine.SubmitChanges();

            txtName.Text = "";
            txtURL.Text = "";
            txtDescription.Text = "";
            GridView1.DataBind();
            GridView1.DataBind();
            updatePanel1.Update();

            Utilities utilities = new Utilities();
            Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
            Literal links = panel.FindControl("linksDp") as Literal;
            links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                GridView1.AllowPaging = false;
                LinqEstablishmentTypes.DataBind();
                LinqEstablishmentTypes.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
            else
            {
                GridView1.AllowPaging = true;
                GridView1.PageSize = int.Parse(ddlSize.SelectedValue);
                LinqEstablishmentTypes.DataBind();
                LinqEstablishmentTypes.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Name") && !ddlEstablishmentTypes.SelectedValue.Equals(""))
            {
                LinqEstSubTypes.Where = "EstablishmentTypeID = " + ddlEstablishmentTypes.SelectedValue +
                    " && Name.Contains(\"" + txtField.Text + "\")";
                LinqEstSubTypes.DataBind();
                Session.Add("whereEstSubType", LinqEstSubTypes.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
            else
            {
                LinqEstSubTypes.Where = "EstablishmentSubTypeID == " + txtField.Text;
                LinqEstSubTypes.DataBind();
                Session.Add("whereEstSubType", LinqEstSubTypes.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            LinqEstablishmentTypes.Where = "";
            LinqEstablishmentTypes.DataBind();
            Session["whereEstSubType"] = "";
            GridView1.DataBind();
            GridView1.DataBind();
            updatePanel1.Update();
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_EstablishmentSubTypes where a.EstablishmentSubTypeID == key select a;
            try
            {
                dine.DS_EstablishmentSubTypes.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqEstablishmentTypes.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }
    }
}
