﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Establishments.aspx.cs" Inherits="DineSelect.Establishments" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" language="javascript">
        function Delete(val) {
            document.getElementById("<%= deleteHidden.ClientID %>").value = val;
            document.getElementById('<%=btnOne.ClientID %>').click();
        }

    </script>

    <asp:Button ID="btnOne" runat="server" Text="One" Style="display: none;" OnClick="btnOne_Click" />
    <asp:Label ID="Label1" Width="100%" runat="server" Text="Establishments" CssClass="labelTop"></asp:Label>
    <table>
        <tr>
            <td>
                <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <input type="hidden" id="deleteHidden" runat="server" />
                        <br />
                        <br />
                        <table>
                            <tr>
                                <td>
                                    Rows to Display
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSize" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSize_SelectedIndexChanged">
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                        <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                        <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Search for:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlField" runat="server">
                                        <asp:ListItem Text="Name" Value="Name"></asp:ListItem>
                                        <asp:ListItem Text="ID" Value="ID"></asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtField" runat="server" Width="300px"></asp:TextBox>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="linkSearch" runat="server" OnClick="linkSearch_Click">Search</asp:LinkButton>
                                    &nbsp;
                                    <asp:LinkButton ID="linkClear" runat="server" OnClick="linkClear_Click">Clear</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table width="100%">
                            <tr>
                                <td style="width: 165px">
                                    Please choose a country:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCountries" runat="server" AutoPostBack="True" DataTextField="Name"
                                        DataValueField="CountryID" Style="height: 22px" AppendDataBoundItems="true" Width="250px"
                                        OnSelectedIndexChanged="ddlCountries_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 165px">
                                    Please choose a province:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlProvinces" runat="server" OnSelectedIndexChanged="ddlProvinces_SelectedIndexChanged"
                                        AutoPostBack="True" Style="height: 22px" Width="250px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 165px">
                                    Please choose a city:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCities" runat="server" AutoPostBack="True" Style="height: 22px"
                                        Width="250px" OnSelectedIndexChanged="ddlCities_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 165px">
                                    Please choose a region:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlRegions" runat="server" AutoPostBack="True" Style="height: 22px"
                                        Width="250px" OnSelectedIndexChanged="ddlRegions_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 165px">
                                    Please choose a locality:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlLocalities" runat="server" AutoPostBack="True" Style="height: 22px"
                                        Width="250px" OnSelectedIndexChanged="ddlLocalities_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="hidden" runat="server" id="hiddenValue" />
                                    <br />
                                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                    <br />
                                    <asp:ListView ID="ListView1" runat="server" DataKeyNames="EstablishmentID" DataSourceID="LinqEstablishments">
                                        <ItemTemplate>
                                            <tr class="RowStyle">
                                                <td>
                                                    <asp:HyperLink ID="HyperLink1" NavigateUrl='<%# "ViewEditEstablishment.aspx?id=" + Eval("EstablishmentID") + "&mode=edit" %>'
                                                        Text="Edit" runat="server"></asp:HyperLink>
                                                    <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="DeleteButton"
                                                        DisplayModalPopupID="ModalPopupExtender1" />
                                                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="DeleteButton"
                                                        PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                                    <asp:Panel ID="PNL" runat="server" Style="display: none; width: 200px; background-color: White;
                                                        border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                                        Are you sure you want to delete this item?
                                                        <br />
                                                        <br />
                                                        <div style="text-align: right;">
                                                            <asp:Button ID="ButtonOk" runat="server" Text="OK" OnClientClick='<%# "Delete(" + Eval("EstablishmentID") + ");"%>' />
                                                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                                <td>
                                                    <asp:DynamicControl ID="DynamicControl2" runat="server" DataField="Name" Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl ID="DynamicControl3" runat="server" DataField="DS_CityRegion"
                                                        Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl ID="DynamicControl4" runat="server" DataField="DS_Country" Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl runat="server" DataField="Address" Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:HyperLink ID="linkView" NavigateUrl='<%# "ViewEditEstablishment.aspx?id=" + Eval("EstablishmentID") + "&mode=view" %>'
                                                        Text="View All Info" runat="server"></asp:HyperLink>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <tr class="AltRowStyle">
                                                <td style="width: 70px">
                                                    <asp:HyperLink ID="HyperLink2" NavigateUrl='<%# "ViewEditEstablishment.aspx?id=" + Eval("EstablishmentID") + "&mode=edit" %>'
                                                        Text="Edit" runat="server"></asp:HyperLink>
                                                    <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="DeleteButton"
                                                        DisplayModalPopupID="ModalPopupExtender1" />
                                                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="DeleteButton"
                                                        PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                                    <asp:Panel ID="PNL" runat="server" Style="display: none; width: 200px; background-color: White;
                                                        border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                                        Are you sure you want to delete this item?
                                                        <br />
                                                        <br />
                                                        <div style="text-align: right;">
                                                            <asp:Button ID="ButtonOk" runat="server" Text="OK" OnClientClick='<%# "Delete(" + Eval("EstablishmentID") + ");"%>' />
                                                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                                <td>
                                                    <asp:DynamicControl ID="DynamicControl1" runat="server" DataField="Name" Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl ID="DynamicControl5" runat="server" DataField="DS_CityRegion"
                                                        Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl ID="DynamicControl6" runat="server" DataField="DS_Country" Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:DynamicControl runat="server" DataField="Address" Mode="ReadOnly" />
                                                </td>
                                                <td>
                                                    <asp:HyperLink ID="linkView" NavigateUrl='<%# "ViewEditEstablishment.aspx?id=" + Eval("EstablishmentID") + "&mode=view" %>'
                                                        Text="View All Info" runat="server"></asp:HyperLink>
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                        <EmptyDataTemplate>
                                            <table runat="server" class="GridViewStyle">
                                                <tr>
                                                    <td>
                                                        No data was returned.
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                        <LayoutTemplate>
                                            <table runat="server" class="GridViewStyle">
                                                <tr runat="server">
                                                    <td runat="server">
                                                        <table id="itemPlaceholderContainer" runat="server" border="0" style="">
                                                            <tr runat="server" class="HeaderStyle">
                                                                <th runat="server">
                                                                </th>
                                                                <th runat="server">
                                                                    Name
                                                                </th>
                                                                <th id="Th1" runat="server">
                                                                    City Region
                                                                </th>
                                                                <th id="Th2" runat="server">
                                                                    Country
                                                                </th>
                                                                <th runat="server">
                                                                    Address
                                                                </th>
                                                                <th>
                                                                </th>
                                                            </tr>
                                                            <tr runat="server" id="itemPlaceholder">
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr runat="server">
                                                    <td runat="server" style="">
                                                        <asp:DataPager ID="DataPager1" runat="server" PageSize="5">
                                                            <Fields>
                                                                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                                                    ShowPreviousPageButton="False" />
                                                                <asp:NumericPagerField />
                                                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                                                    ShowPreviousPageButton="False" />
                                                            </Fields>
                                                        </asp:DataPager>
                                                    </td>
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                    </asp:ListView>
                                    <asp:LinqDataSource ID="LinqEstablishments" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                                        EnableDelete="True" EnableInsert="True" EnableUpdate="True" OrderBy="Name" 
                                        TableName="DS_Establishments" Where="CityID == @CityID">
                                        <WhereParameters>
                                            <asp:Parameter DefaultValue="0" Name="CityID" Type="Int32" />
                                        </WhereParameters>
                                    </asp:LinqDataSource>
                                    <br />
                                    <asp:HyperLink NavigateUrl="~/ViewEditEstablishment.aspx?mode=insert" Text="Insert new Establishment"
                                        runat="server"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="update">
                    <ProgressTemplate>
                        <img id="Img1" src="images/wait.gif" width="40" height="40" runat="server" alt="Loading" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
</asp:Content>
