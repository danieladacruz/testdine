﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class Establishments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (!Page.IsPostBack)
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                var countries = from c in dine.DS_Countries
                                orderby c.Name
                                select c;
                ddlCountries.DataSource = countries;
                ddlCountries.DataTextField = "Name";
                ddlCountries.DataValueField = "CountryID";
                if (countries.Count() > 1)
                {
                    ddlCountries.Items.Insert(0, new ListItem("", ""));
                }
                else if (countries.Count() == 1)
                {
                    BindProvincesForCountry(countries.First().CountryID);

                    ListView1.DataBind();

                }
                ddlCountries.DataBind();
            }
            else
            {
                LinqEstablishments.Where = Session["whereEstablishments"] + "";
                LinqEstablishments.DataBind();
            }
        }

        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0"))
            {
                int country = int.Parse(ddlCountries.SelectedValue);
                BindProvincesForCountry(country);
                //LinqEstablishments.Where = "CountryID == " + int.Parse(ddlCountries.SelectedValue);
                //LinqEstablishments.DataBind();
            }

        }


        protected void ddlProvinces_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0"))
            {
                int province = int.Parse(ddlProvinces.SelectedValue);
                BindCitiesForProvince(province);
                //LinqEstablishments.Where = "CountryID == " + int.Parse(ddlCountries.SelectedValue) +
                //                          " && ProvinceID == " + province;
                //LinqEstablishments.DataBind();
            }
        }



        protected void ddlCities_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0") &&
                !ddlCities.SelectedValue.Equals("") && !ddlCities.SelectedValue.Equals("0"))
            {
                int city = int.Parse(ddlCities.SelectedValue);
                BindRegionsForCity(city);
                //LinqEstablishments.Where = "CountryID == " + int.Parse(ddlCountries.SelectedValue) +
                //                          " && ProvinceID == " + int.Parse(ddlProvinces.SelectedValue) +
                //                          " && CityID == " + city;
                //LinqEstablishments.DataBind();
            }
        }

        protected void ddlRegions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0") &&
                !ddlCities.SelectedValue.Equals("") && !ddlCities.SelectedValue.Equals("0") &&
                !ddlRegions.SelectedValue.Equals("") && !ddlRegions.SelectedValue.Equals("0"))
            {
                int region = int.Parse(ddlRegions.SelectedValue);
                BindLocalitiesForRegion(region);
                LinqEstablishments.Where = "CountryID == " + int.Parse(ddlCountries.SelectedValue) +
                                           " && ProvinceID == " + int.Parse(ddlProvinces.SelectedValue) +
                                           " && CityID == " + int.Parse(ddlCities.SelectedValue) +
                                           " && CityRegionID == " + region;
                ListView1.DataBind();
            }
        }

        protected void ddlLocalities_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                   !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0") &&
                   !ddlCities.SelectedValue.Equals("") && !ddlCities.SelectedValue.Equals("0") &&
                   !ddlRegions.SelectedValue.Equals("") && !ddlRegions.SelectedValue.Equals("0") &&
                   !ddlLocalities.SelectedValue.Equals("") && !ddlLocalities.SelectedValue.Equals("0"))
            {
                int locality = int.Parse(ddlLocalities.SelectedValue);
                LinqEstablishments.Where = "CountryID == " + int.Parse(ddlCountries.SelectedValue) +
                                           " && ProvinceID == " + int.Parse(ddlProvinces.SelectedValue) +
                                           " && CityID == " + int.Parse(ddlCities.SelectedValue) +
                                           " && CityRegionID == " + int.Parse(ddlRegions.SelectedValue) +
                                           " && CityLocalityID == " + locality;
                LinqEstablishments.DataBind();
            }
        }

        private void BindCitiesForProvince(int province)
        {
            ddlCities.Items.Clear();
            ddlRegions.Items.Clear();
            ddlLocalities.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var cities = from c in dine.DS_Cities
                         where c.ProvinceID == province
                         select c;

            ddlCities.DataSource = cities;
            ddlCities.DataTextField = "Name";
            ddlCities.DataValueField = "provinceID";
            ddlCities.AppendDataBoundItems = true;
            if (cities.Count() > 1)
            {
                ddlCities.Items.Insert(0, new ListItem("", ""));
            }
            else if (cities.Count() == 1)
            {
                BindRegionsForCity(cities.First().CityID);
            }
            ddlCities.DataBind();
        }

        private void BindRegionsForCity(int city)
        {
            ddlRegions.Items.Clear();
            ddlLocalities.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var regions = from c in dine.DS_CityRegions
                          where c.CityID == city
                          select c;

            ddlRegions.DataSource = regions;
            ddlRegions.DataTextField = "Name";
            ddlRegions.DataValueField = "CityRegionID";
            ddlRegions.AppendDataBoundItems = true;
            if (regions.Count() > 1)
            {
                ddlRegions.Items.Insert(0, new ListItem("", ""));
            }
            else if (regions.Count() == 1)
            {
                BindLocalitiesForRegion(regions.First().CityRegionID);
            }
            ddlRegions.DataBind();
        }

        private void BindLocalitiesForRegion(int region)
        {
            ddlLocalities.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var intersections = from c in dine.DS_CityLocalities
                                where c.CityRegionID == region
                                select c;

            ddlLocalities.DataSource = intersections;
            ddlLocalities.DataTextField = "Name";
            ddlLocalities.DataValueField = "CityLocalityID";
            ddlLocalities.AppendDataBoundItems = true;
            if (intersections.Count() > 1)
            {
                ddlLocalities.Items.Insert(0, new ListItem("", ""));
            }

            ddlLocalities.DataBind();
        }

        private void BindProvincesForCountry(int country)
        {
            ddlProvinces.Items.Clear();
            ddlCities.Items.Clear();
            ddlRegions.Items.Clear();
            ddlLocalities.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var provinces = from p in dine.DS_Provinces
                            where p.CountryID == country
                            select p;
            ddlProvinces.DataSource = provinces;
            ddlProvinces.DataTextField = "Name";
            ddlProvinces.DataValueField = "provinceID";
            ddlProvinces.AppendDataBoundItems = true;
            if (provinces.Count() > 1)
            {
                ddlProvinces.Items.Insert(0, new ListItem("", ""));
            }
            else if (provinces.Count() == 1)
            {
                BindCitiesForProvince(provinces.First().ProvinceID);


            }
            ddlProvinces.DataBind();
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                DataPager pgr = ListView1.FindControl("DataPager1") as DataPager;
                if (pgr != null)
                {
                    pgr.SetPageProperties(0, pgr.TotalRowCount, false);
                }
                LinqEstablishments.DataBind();
                LinqEstablishments.DataBind();
                ListView1.DataBind();
                ListView1.DataBind();
                update.Update();
            }
            else
            {
                DataPager pgr = ListView1.FindControl("DataPager1") as DataPager;
                if (pgr != null && ListView1.Items.Count != pgr.TotalRowCount)
                {
                    pgr.SetPageProperties(0, int.Parse(ddlSize.SelectedValue), false);
                }
                LinqEstablishments.DataBind();
                ListView1.DataBind();
                ListView1.DataBind();
                update.Update();
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Name"))
            {
                LinqEstablishments.Where = "Name.Contains(\"" + txtField.Text + "\")";
                LinqEstablishments.DataBind();
                Session.Add("whereEstablishments", LinqEstablishments.Where);
                ListView1.DataBind();
                ListView1.DataBind();
                update.Update();
            }
            else
            {
                LinqEstablishments.Where = "EstablishmentID == " + txtField.Text;
                LinqEstablishments.DataBind();
                Session.Add("whereEstablishments", LinqEstablishments.Where);
                ListView1.DataBind();
                ListView1.DataBind();
                update.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            LinqEstablishments.Where = "";
            LinqEstablishments.DataBind();
            Session["whereEstablishments"] = "";
            ListView1.DataBind();
            ListView1.DataBind();
            update.Update();
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_Establishments where a.EstablishmentID == key select a;
            try
            {
                dine.DS_Establishments.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqEstablishments.DataBind();
                ListView1.DataBind();
                ListView1.DataBind();
                update.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }

    }
}
