﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Images.aspx.cs" Inherits="DineSelect.Images" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" language="javascript">
        function Delete(val) {
            document.getElementById("<%= deleteHidden.ClientID %>").value = val;
            document.getElementById('<%=btnOne.ClientID %>').click();
        }

    </script>

    <asp:Button ID="btnOne" runat="server" Text="One" Style="display: none;" OnClick="btnOne_Click" />
    <asp:Label ID="Label1" Width="100%" runat="server" Text="Establishment Images" CssClass="labelTop"></asp:Label>
    <br />
    <table width="100%">
        <tr>
            <td style="width: 90%">
                <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <input type="hidden" id="deleteHidden" runat="server" />
                        <table width="100%">
                            <tr>
                                <td style="width: 165px">
                                    Please choose an establishment:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlEstablishments" runat="server" AutoPostBack="True" Style="height: 22px"
                                        AppendDataBoundItems="True" Width="350px" DataSourceID="LinqEstablishments" DataTextField="Name"
                                        DataValueField="EstablishmentID" OnSelectedIndexChanged="ddlEstablishments_SelectedIndexChanged">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:LinqDataSource ID="LinqEstablishments" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                                        OrderBy="Name" Select="new (Name, EstablishmentID)" TableName="DS_Establishments">
                                    </asp:LinqDataSource>
                                </td>
                            </tr>
                        </table>
                        <b>New Image</b>
                        <asp:Panel ID="panelAdd" runat="server" Enabled="false">
                            <table class="GridViewStyle">
                                <tr class="HeaderStyle">
                                    <th align="left">
                                        Select the image
                                    </th>
                                    <td>
                                        <asp:FileUpload ID="fileUpload" runat="server" Width="100%" />
                                    </td>
                                </tr>
                                <tr class="HeaderStyle">
                                    <th align="left">
                                        Description
                                    </th>
                                    <td>
                                        <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="HeaderStyle">
                                    <th align="left">
                                        Enabled
                                    </th>
                                    <td>
                                        <asp:CheckBox ID="checkEnabled" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right">
                                        <asp:LinkButton ID="linkSave" runat="server" OnClick="linkSave_Click" ValidationGroup="add">Save</asp:LinkButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="linkSave"
                                            DisplayModalPopupID="ModalPopupExtender1" ConfirmOnFormSubmit="true" />
                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="linkSave"
                                            PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                        <asp:Panel ID="PNL" runat="server" Style="display: none; width: 250px; background-color: White;
                                            border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                            Are you sure you want to save this item?
                                            <br />
                                            <br />
                                            <div style="text-align: right;">
                                                <asp:Button ID="ButtonOk" runat="server" Text="OK" />
                                                <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <br />
                        <br />
                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                        <br />
                        <b>Existent Images</b>
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ImageID"
                            DataSourceID="LinqImages" AllowPaging="True" CssClass="GridViewStyle" AlternatingRowStyle-CssClass="RowStyle"
                            RowStyle-CssClass="RowStyle" HeaderStyle-CssClass="HeaderStyle" EditRowStyle-CssClass="EditRowStyle">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="linkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                                        <asp:LinkButton ID="DeleteButton" runat="server">Delete</asp:LinkButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="DeleteButton"
                                            DisplayModalPopupID="ModalPopupExtender1" />
                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="DeleteButton"
                                            PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                        <asp:Panel ID="PNL" runat="server" Style="display: none; width: 200px; background-color: White;
                                            border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                            Are you sure you want to delete this item?
                                            <br />
                                            <br />
                                            <div style="text-align: right;">
                                                <asp:Button ID="ButtonOk" runat="server" Text="OK" OnClientClick='<%# "Delete(" + Eval("ImageID") + ");"%>' />
                                                <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                            </div>
                                        </asp:Panel>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="linkUpdate" runat="server" CommandName="Update">Update</asp:LinkButton>
                                        <asp:LinkButton ID="linkCancel" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <img src='<%# "Thumbnail.ashx?p=" + Eval("ImageURL") %>' alt='<%# Eval("ImageURL") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
                                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                                <asp:CheckBoxField DataField="Enabled" HeaderText="Enabled" SortExpression="Enabled" />
                            </Columns>
                            <EmptyDataTemplate>
                                No images were returned.</EmptyDataTemplate>
                        </asp:GridView>
                        <asp:LinqDataSource ID="LinqImages" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                            EnableDelete="True" EnableInsert="True" EnableUpdate="True" OrderBy="ImageID"
                            TableName="DS_Images" Where="EstablishmentID == @EstablishmentID">
                            <WhereParameters>
                                <asp:ControlParameter ControlID="ddlEstablishments" Name="EstablishmentID" PropertyName="SelectedValue"
                                    Type="Int32" DefaultValue="0" />
                            </WhereParameters>
                        </asp:LinqDataSource>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="linkSave" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
            <td valign="top">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
                    <ProgressTemplate>
                        <img id="Img1" src="images/wait.gif" width="40" height="40" runat="server" alt="Loading" /></ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
</asp:Content>
