﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace DineSelect
{
    public partial class Images : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
            }
        }

        protected void ddlEstablishments_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlEstablishments.SelectedValue.Equals(""))
            {
                panelAdd.Enabled = true;
            }
            else
            {
                panelAdd.Enabled = false;
            }
        }

        private int getMaximumNumberFromFolder(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo[] rgFiles = di.GetFiles("*.*");
            List<int> numFiles = new List<int>();

            foreach (FileInfo fi in rgFiles)
            {
                string name = Path.GetFileNameWithoutExtension(fi.Name);
                name = name.Replace("Image", "");
                int n = int.Parse(name);
                numFiles.Add(n);
            }
            return (numFiles.Count() > 0 ? numFiles.Max() + 1 : 1);
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                
                var establs = from es in dine.DS_Establishments
                              where es.EstablishmentID == int.Parse(ddlEstablishments.SelectedValue)
                              select es;
                DS_Establishment est = establs.First();
                string folder_name = est.Name.Replace("&", "").Replace(" ", "").Replace("'","");

                string path = Server.MapPath("~/Pictures");
                path = path + "//" + folder_name;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                int max = getMaximumNumberFromFolder(path);
                string ext = Path.GetExtension(fileUpload.FileName);
                string img_name = "Image" + max + ext;
                path = path + "//" + img_name;

                fileUpload.SaveAs(path);

                DS_Image img = new DS_Image();
                img.Description = txtDescription.Text;
                img.Enabled = checkEnabled.Checked;
                img.EstablishmentID = int.Parse(ddlEstablishments.SelectedValue);
                img.ImageURL = folder_name  + "/" + img_name;
                dine.DS_Images.InsertOnSubmit(img);
                dine.SubmitChanges();

                LinqImages.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_Images where a.ImageID == key select a;
            try
            {
                dine.DS_Images.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqImages.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }
    }
}
