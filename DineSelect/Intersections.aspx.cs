﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class Intersections : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (!Page.IsPostBack)
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                var countries = from c in dine.DS_Countries
                                orderby c.Name
                                select c;
                ddlCountries.DataSource = countries;
                ddlCountries.DataTextField = "Name";
                ddlCountries.DataValueField = "CountryID";
                if (countries.Count() > 1)
                {
                    ddlCountries.Items.Insert(0, new ListItem("", "0"));
                }
                else if (countries.Count() == 1)
                {
                    BindProvincesForCountry(countries.First().CountryID);
                }
                ddlCountries.DataBind();

                LinqIntersections.Where = Session["whereIntersections"] + "";
                LinqIntersections.DataBind();
            }
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0") &&
                !ddlCities.SelectedValue.Equals("") && !ddlCities.SelectedValue.Equals("0") &&
                !ddlRegions.SelectedValue.Equals("") && !ddlRegions.SelectedValue.Equals("0") &&
                !ddlLocalities.SelectedValue.Equals("") && !ddlLocalities.SelectedValue.Equals("0"))
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                DS_Intersection intersection = new DS_Intersection();
                intersection.CityLocalityID = int.Parse(ddlLocalities.SelectedValue);
                intersection.Description = txtDescription.Text;
                intersection.Name = txtName.Text;
                if (!txtIntType.Text.Equals(""))
                {
                    intersection.IntersectionType = byte.Parse(txtIntType.Text);
                }
                dine.DS_Intersections.InsertOnSubmit(intersection);
                dine.SubmitChanges();

                txtName.Text = "";
                txtDescription.Text = "";
                txtIntType.Text = "";
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
        }

        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0"))
            {
                BindProvincesForCountry(int.Parse(ddlCountries.SelectedValue));
            }
        }


        protected void ddlProvinces_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0"))
            {
                BindCitiesForProvince(int.Parse(ddlProvinces.SelectedValue));
            }
        }



        protected void ddlCities_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0") &&
                !ddlCities.SelectedValue.Equals("") && !ddlCities.SelectedValue.Equals("0"))
            {
                BindRegionsForCity(int.Parse(ddlCities.SelectedValue));
            }
        }

        protected void ddlRegions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0") &&
                !ddlCities.SelectedValue.Equals("") && !ddlCities.SelectedValue.Equals("0") &&
                !ddlRegions.SelectedValue.Equals("") && !ddlRegions.SelectedValue.Equals("0"))
            {
                BindLocalitiesForRegion(int.Parse(ddlRegions.SelectedValue));
            }
        }

        private void BindCitiesForProvince(int province)
        {
            ddlCities.Items.Clear();
            ddlRegions.Items.Clear();
            ddlLocalities.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var cities = from c in dine.DS_Cities
                         where c.ProvinceID == province
                         select c;

            ddlCities.DataSource = cities;
            ddlCities.DataTextField = "Name";
            ddlCities.DataValueField = "cityID";
            ddlCities.AppendDataBoundItems = true;
            if (cities.Count() > 1)
            {
                ddlCities.Items.Insert(0, new ListItem("", "0"));
            }
            else if (cities.Count() == 1)
            {
                BindRegionsForCity(cities.First().CityID);
            }
            ddlCities.DataBind();
        }

        private void BindRegionsForCity(int city)
        {
            ddlRegions.Items.Clear();
            ddlLocalities.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var regions = from c in dine.DS_CityRegions
                          where c.CityID == city
                          select c;

            ddlRegions.DataSource = regions;
            ddlRegions.DataTextField = "Name";
            ddlRegions.DataValueField = "CityRegionID";
            ddlRegions.AppendDataBoundItems = true;
            if (regions.Count() > 1)
            {
                ddlRegions.Items.Insert(0, new ListItem("", "0"));
            }
            else if (regions.Count() == 1)
            {
                BindLocalitiesForRegion(regions.First().CityRegionID);
            }
            ddlRegions.DataBind();
        }

        private void BindLocalitiesForRegion(int region)
        {
            ddlLocalities.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var intersections = from c in dine.DS_CityLocalities
                                where c.CityRegionID == region
                                select c;

            ddlLocalities.DataSource = intersections;
            ddlLocalities.DataTextField = "Name";
            ddlLocalities.DataValueField = "CityLocalityID";
            ddlLocalities.AppendDataBoundItems = true;
            if (intersections.Count() > 1)
            {
                ddlLocalities.Items.Insert(0, new ListItem("", "0"));
            }
            else
            {
                panelAdd.Enabled = true;
            }

            ddlLocalities.DataBind();
        }

        private void BindProvincesForCountry(int country)
        {
            ddlProvinces.Items.Clear();
            ddlCities.Items.Clear();
            ddlRegions.Items.Clear();
            ddlLocalities.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var provinces = from p in dine.DS_Provinces
                            where p.CountryID == country
                            select p;
            ddlProvinces.DataSource = provinces;
            ddlProvinces.DataTextField = "Name";
            ddlProvinces.DataValueField = "provinceID";
            ddlProvinces.AppendDataBoundItems = true;
            if (provinces.Count() > 1)
            {
                ddlProvinces.Items.Insert(0, new ListItem("", "0"));
            }
            else if (provinces.Count() == 1)
            {
                BindCitiesForProvince(provinces.First().ProvinceID);
            }
            ddlProvinces.DataBind();
        }

        protected void ddlLocalities_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlLocalities.SelectedValue.Equals(""))
            {
                panelAdd.Enabled = true;
            }
            else
            {
                panelAdd.Enabled = false;
            }
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                GridView1.AllowPaging = false;
                LinqIntersections.DataBind();
                LinqIntersections.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            else
            {
                GridView1.AllowPaging = true;
                GridView1.PageSize = int.Parse(ddlSize.SelectedValue);
                LinqIntersections.DataBind();
                LinqIntersections.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Name"))
            {
                LinqIntersections.Where = "Name.Contains(\"" + txtField.Text + "\")";
                LinqIntersections.DataBind();
                Session.Add("whereIntersections", LinqIntersections.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            else
            {
                LinqIntersections.Where = "IntersectionID == " + txtField.Text;
                LinqIntersections.DataBind();
                Session.Add("whereIntersections", LinqIntersections.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            LinqIntersections.Where = "";
            LinqIntersections.DataBind();
            Session["whereIntersections"] = "";
            GridView1.DataBind();
            GridView1.DataBind();
            UpdatePanel1.Update();
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_Intersections where a.IntersectionID == key select a;
            try
            {
                dine.DS_Intersections.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqIntersections.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }
    }
}
