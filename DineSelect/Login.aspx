﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="DineSelect.Login" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <asp:Login ID="Login1" runat="server" LoginButtonText="Login" LoginButtonType="Link"
        TitleText="Login" DestinationPageUrl="~/Default.aspx">
        <LayoutTemplate>
            <table id="rounded-corner">
                <thead>
                    <tr>
                        <th scope="col" class="rounded-company" style="height: 0px">
                        </th>
                        <th>
                            Login
                        </th>
                        <th scope="col" class="rounded-q4">
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td class="rounded-foot-left" align="center">
                        </td>
                        <td align="center">
                            <asp:Button ID="Login" CommandName="Login" runat="server" Text="Login"></asp:Button>
                            <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                        </td>
                        <td class="rounded-foot-right">
                        </td>
                    </tr>
                </tfoot>
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td>
                                    Username:
                                </td>
                                <td>
                                    <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                        Text="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Password:
                                </td>
                                <td>
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                        Text="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:CheckBox ID="RememberMe" runat="server" Text="Remember next time"></asp:CheckBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
    </asp:Login>
    <br />
    <table>
        <tr>
            <td>
                <asp:HyperLink ID="link1" runat="server" Text="Recover my password" NavigateUrl="~/ContactForm.aspx?mode=recover"></asp:HyperLink>
            </td>
            <td>
                <asp:HyperLink ID="HyperLink1" runat="server" Text="Ask for more privileges" NavigateUrl="~/ContactForm.aspx?mode=ask"></asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>
