﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class Provinces : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (!Page.IsPostBack)
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                var countries = from c in dine.DS_Countries
                                orderby c.Name
                                select c;
                ddlCountries.DataSource = countries;
                ddlCountries.DataTextField = "name";
                ddlCountries.DataValueField = "countryID";
                ddlCountries.DataBind();

                LinqProvinces.Where = Session["whereProvinces"] + "";
                LinqProvinces.DataBind();
            }
        }

        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals(""))
            {
                DineSelectDataContext dine = new DineSelectDataContext();
                DS_Province province = new DS_Province();
                province.Name = txtName.Text;
                province.ShortName = txtShortName.Text;
                province.CountryID = int.Parse(ddlCountries.SelectedValue);
                province.Description = txtDescription.Text;

                dine.DS_Provinces.InsertOnSubmit(province);
                dine.SubmitChanges();

                txtDescription.Text = "";
                txtName.Text = "";
                txtShortName.Text = "";
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                GridView1.AllowPaging = false;
                LinqProvinces.DataBind();
                LinqProvinces.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            else
            {
                GridView1.AllowPaging = true;
                GridView1.PageSize = int.Parse(ddlSize.SelectedValue);
                LinqProvinces.DataBind();
                LinqProvinces.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Name"))
            {
                LinqProvinces.Where = "Name.Contains(\"" + txtField.Text + "\")";
                LinqProvinces.DataBind();
                Session.Add("whereProvinces", LinqProvinces.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            else
            {
                LinqProvinces.Where = "ProvinceID == " + txtField.Text;
                Session.Add("whereProvinces", LinqProvinces.Where);
                LinqProvinces.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            LinqProvinces.Where = "";
            LinqProvinces.DataBind();
            Session["whereProvinces"] = "";
            GridView1.DataBind();
            GridView1.DataBind();
            UpdatePanel1.Update();
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_Provinces where a.ProvinceID == key select a;
            try
            {
                dine.DS_Provinces.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqProvinces.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("PanelDp1") as Panel;
                Literal links = panel.FindControl("linksDp") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }
    }
}
