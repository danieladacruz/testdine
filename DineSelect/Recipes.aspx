<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Recipes.aspx.cs" Inherits="DineSelect.Recipes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" language="javascript">
        function Delete(val) {
            document.getElementById("<%= deleteHidden.ClientID %>").value = val;
            document.getElementById('<%=btnOne.ClientID %>').click();
        }

    </script>

    <asp:Button ID="btnOne" runat="server" Text="One" Style="display: none;" OnClick="btnOne_Click" />
    <asp:Label ID="Label1" Width="100%" runat="server" Text="Recipes" CssClass="labelTop"></asp:Label>
    <table>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <input type="hidden" id="deleteHidden" runat="server" />
                        <br />
                        <br />
                        <table>
                            <tr>
                                <td>
                                    Rows to Display
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSize" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSize_SelectedIndexChanged">
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                        <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                        <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Search for:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlField" runat="server">
                                        <asp:ListItem Text="Name" Value="Name"></asp:ListItem>
                                        <asp:ListItem Text="ID" Value="ID"></asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtField" runat="server" Width="300px"></asp:TextBox>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="linkSearch" runat="server" OnClick="linkSearch_Click">Search</asp:LinkButton>
                                    &nbsp;
                                    <asp:LinkButton ID="linkClear" runat="server" OnClick="linkClear_Click">Clear</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table width="100%">
                            <tr>
                                <td style="width: 165px">
                                    Please choose a recipe type:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlRecipes" runat="server" AutoPostBack="True" DataTextField="Name"
                                        DataValueField="RecipeTypeID" Style="height: 22px" AppendDataBoundItems="True"
                                        Width="250px" DataSourceID="LinqRecipeTypes" OnSelectedIndexChanged="ddlRecipes_SelectedIndexChanged">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:LinqDataSource ID="LinqRecipeTypes" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                                        OrderBy="Name" TableName="DS_RecipeTypes">
                                    </asp:LinqDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                    <br />
                                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                                        DataSourceID="LinqRecipes" CssClass="GridViewStyle" RowStyle-CssClass="RowStyle"
                                        AlternatingRowStyle-CssClass="AltRowStyle" HeaderStyle-CssClass="HeaderStyle"
                                        AutoGenerateColumns="False" DataKeyNames="RecipeID">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="HyperLink1" NavigateUrl='<%# "ViewEditRecipe.aspx?id=" + Eval("recipeID") + "&mode=edit" %>'
                                                        Text="Edit" runat="server"></asp:HyperLink>
                                                    <asp:LinkButton ID="DeleteButton" runat="server">Delete</asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="DeleteButton"
                                                        DisplayModalPopupID="ModalPopupExtender1" />
                                                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="DeleteButton"
                                                        PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                                    <asp:Panel ID="PNL" runat="server" Style="display: none; width: 200px; background-color: White;
                                                        border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                                        Are you sure you want to delete this item?
                                                        <br />
                                                        <br />
                                                        <div style="text-align: right;">
                                                            <asp:Button ID="ButtonOk" runat="server" Text="OK" OnClientClick='<%# "Delete(" + Eval("RecipeID") + ");"%>' />
                                                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                                        </div>
                                                    </asp:Panel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="RecipeID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                                                SortExpression="RecipeID" />
                                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                                            <asp:BoundField DataField="Servings" HeaderText="Servings" SortExpression="Servings" />
                                            <asp:BoundField DataField="PreparationTimeMinutes" HeaderText="Preparation Time (minutes)"
                                                SortExpression="PreparationTimeMinutes" />
                                            <asp:BoundField DataField="ReadyInTimeMinutes" HeaderText="Ready In (minutes)" SortExpression="ReadyInTimeMinutes" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="linkView" NavigateUrl='<%# "ViewEditRecipe.aspx?id=" + Eval("recipeID") + "&mode=view" %>'
                                                        Text="View All Info" runat="server"></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No data was returned.</EmptyDataTemplate>
                                    </asp:GridView>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="hidden" runat="server" id="hiddenValue" />
                                    <asp:LinqDataSource ID="LinqRecipes" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                                        EnableDelete="True" EnableInsert="True" EnableUpdate="True" OrderBy="Name" TableName="DS_Recipes"
                                        Where="RecipeTypeID == @RecipeTypeID">
                                        <WhereParameters>
                                            <asp:ControlParameter ControlID="ddlRecipes" Name="RecipeTypeID" PropertyName="SelectedValue"
                                                Type="Int32" DefaultValue="0" />
                                        </WhereParameters>
                                    </asp:LinqDataSource>
                                    <br />
                                    <asp:HyperLink ID="HyperLink2" NavigateUrl="~/ViewEditRecipe.aspx?mode=insert" Text="Insert new Recipe"
                                        runat="server"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                    <ProgressTemplate>
                        <img id="Img1" src="images/wait.gif" width="40" height="40" runat="server" alt="Loading" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
</asp:Content>
