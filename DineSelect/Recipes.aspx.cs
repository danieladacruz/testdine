﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class Recipes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                LinqRecipes.Where = Session["whereRecipes"] + "";
                LinqRecipes.DataBind();
            }
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                GridView1.AllowPaging = false;
                LinqRecipes.DataBind();
                LinqRecipes.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            else
            {
                GridView1.AllowPaging = true;
                GridView1.PageSize = int.Parse(ddlSize.SelectedValue);
                LinqRecipes.DataBind();
                LinqRecipes.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Name"))
            {
                LinqRecipes.Where = "Name.Contains(\"" + txtField.Text + "\")";
                LinqRecipes.DataBind();
                Session.Add("whereRecipes", LinqRecipes.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            else
            {
                LinqRecipes.Where = "RecipeID == " + txtField.Text;
                LinqRecipes.DataBind();
                Session.Add("whereRecipes", LinqRecipes.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            LinqRecipes.Where = "";
            LinqRecipes.DataBind();
            Session["whereRecipes"] = "";
            GridView1.DataBind();
            GridView1.DataBind();
            UpdatePanel1.Update();
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_Recipes where a.RecipeID == key select a;
            try
            {
                dine.DS_Recipes.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqRecipes.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                UpdatePanel1.Update();
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }

        protected void ddlRecipes_SelectedIndexChanged(object sender, EventArgs e)
        {
            LinqRecipes.Where = "RecipeTypeID ==" + ddlRecipes.SelectedValue;
            LinqRecipes.DataBind();
            Session.Add("whereRecipes", LinqRecipes.Where);
            GridView1.DataBind();
            GridView1.DataBind();
            UpdatePanel1.Update();
        }
    }
}
