﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="SendJokes.aspx.cs" Inherits="DineSelect.SendJokes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        function onOkEdit() {
            document.getElementById('<%=editJoke.ClientID%>').click();
        }

        function edit(item) {
            document.getElementById('<%=hiddenValue.ClientID%>').value = item;
        }

        function hideModalPopupEdit() {
            var popup = $find('modalPopupEdit');
            popup.hide();
        }

        function Delete(val) {
            document.getElementById("<%= deleteHidden.ClientID %>").value = val;
            document.getElementById('<%=btnOne.ClientID %>').click();
        }
    </script>

    <asp:Button ID="btnOne" runat="server" Text="One" Style="display: none;" OnClick="btnOne_Click" />
    <asp:Label ID="Label2" Width="100%" runat="server" Text="Send Jokes" CssClass="labelTop"></asp:Label>
    <br />
    <table>
        <tr>
            <td>
                <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    Rows to Display
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSize" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSize_SelectedIndexChanged">
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                        <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                        <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Search for:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlField" runat="server">
                                        <asp:ListItem Text="Username" Value="Username"></asp:ListItem>
                                        <asp:ListItem Text="JokeContent" Value="JokeContent"></asp:ListItem>
                                        <asp:ListItem Text="ID" Value="ID"></asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtField" runat="server" Width="300px"></asp:TextBox>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="linkSearch" runat="server" OnClick="linkSearch_Click">Search</asp:LinkButton>
                                    &nbsp;
                                    <asp:LinkButton ID="linkClear" runat="server" OnClick="linkClear_Click">Clear</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" id="deleteHidden" runat="server" />
                        <asp:Button ID="editJoke" runat="server" BackColor="White" BorderStyle="None" Width="0px"
                            Height="1px" OnClick="editJoke_Click" UseSubmitBehavior="false" />
                        <input type="hidden" runat="server" id="hiddenValue" />
                        <br />
                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                        <br />
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" DataKeyNames="JokeID" DataSourceID="LinqSendJokes"
                            HeaderStyle-CssClass="HeaderStyle" AlternatingRowStyle-CssClass="AltRowStyle"
                            RowStyle-CssClass="RowStyle" CssClass="GridViewStyle">
                            <RowStyle CssClass="RowStyle" />
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="linkEditContent" OnClientClick='<%# "edit(" + Eval("JokeID") + ")" %>'
                                            runat="server" CommandName="Select" CausesValidation="false" OnClick="edit_Click">Edit</asp:LinkButton>
                                        <asp:LinkButton ID="DeleteButton" runat="server">Delete</asp:LinkButton>
                                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="DeleteButton"
                                            DisplayModalPopupID="ModalPopupExtender1" />
                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="DeleteButton"
                                            PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                        <asp:Panel ID="PNL" runat="server" Style="display: none; width: 200px; background-color: White;
                                            border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                            Are you sure you want to delete this item?
                                            <br />
                                            <br />
                                            <div style="text-align: right;">
                                                <asp:Button ID="ButtonOk" runat="server" Text="OK" OnClientClick='<%# "Delete(" + Eval("JokeID") + ");"%>' />
                                                <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                            </div>
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="JokeID" HeaderText="ID" SortExpression="JokeID" />
                                <asp:BoundField DataField="Jokeviewcode" HeaderText="Jokeviewcode" SortExpression="Jokeviewcode" />
                                <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" />
                                <asp:BoundField DataField="FriendName" HeaderText="FriendName" SortExpression="FriendName" />
                                <asp:BoundField DataField="FriendEmailID" HeaderText="FriendEmailID" SortExpression="FriendEmailID" />
                                <asp:TemplateField HeaderText="JokeContent">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="linkViewContent" runat="server" CausesValidation="false">View</asp:LinkButton>
                                        <asp:Panel ID="Panel1" runat="server" Style="display: none" CssClass="modalPopup">
                                            <asp:Panel ID="Panel3" runat="server" Style="cursor: move; background-color: #DDDDDD;
                                                border: solid 1px Gray; color: Black">
                                                <div>
                                                    <p>
                                                        Content of the Joke:</p>
                                                </div>
                                            </asp:Panel>
                                            <div>
                                                <br />
                                                <center>
                                                    <asp:Label BackColor="White" Width="100%" ID="jokeContent" runat="server" Text='<%# Eval("JokeContent") %>'></asp:Label>
                                                    <br />
                                                    <br />
                                                    <p style="text-align: center;">
                                                        <asp:Button ID="OkButton" runat="server" Text="OK" />
                                                    </p>
                                                </center>
                                            </div>
                                        </asp:Panel>
                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender" runat="server" TargetControlID="linkViewContent"
                                            PopupControlID="Panel1" BackgroundCssClass="modalBackground" OkControlID="OkButton"
                                            DropShadow="true" PopupDragHandleControlID="Panel3" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="JokeContent" HeaderText="JokeContent" SortExpression="JokeContent" />--%>
                            </Columns>
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AltRowStyle" />
                            <EmptyDataTemplate>
                                No data was returned.</EmptyDataTemplate>
                        </asp:GridView>
                        <asp:LinqDataSource ID="LinqSendJokes" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                            EnableDelete="True" EnableInsert="True" EnableUpdate="True" OrderBy="FriendName"
                            TableName="DS_SendJokes">
                        </asp:LinqDataSource>
                        <asp:Panel ID="PanelEditContent" runat="server" Style="display: none" CssClass="modalPopup"
                            Width="400px">
                            <asp:Panel ID="PanelEditContentDrag" runat="server" Style="cursor: move; background-color: #DDDDDD;
                                border: solid 1px Gray; color: Black">
                                <div>
                                    <p>
                                        Edit the following SendJoke:</p>
                                </div>
                            </asp:Panel>
                            <div>
                                <br />
                                <br />
                                <center>
                                    <table class="GridViewStyle">
                                        <tr class="HeaderStyle">
                                            <th align="left">
                                                JokeViewCode
                                            </th>
                                            <td>
                                                <asp:TextBox Width="200px" ID="txtJokeViewCode" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="HeaderStyle">
                                            <th align="left">
                                                Username
                                            </th>
                                            <td>
                                                <asp:TextBox Width="200px" ID="txtUsername" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                    ControlToValidate="txtUsername" ValidationGroup="edit"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr class="HeaderStyle">
                                            <th align="left">
                                                FriendName
                                            </th>
                                            <td>
                                                <asp:TextBox Width="200px" ID="txtFriendName" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="HeaderStyle">
                                            <th align="left">
                                                FriendEmailID
                                            </th>
                                            <td>
                                                <asp:TextBox Width="200px" ID="txtFriendEmailID" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="HeaderStyle">
                                            <th align="left">
                                                JokeContent
                                            </th>
                                            <td>
                                                <asp:TextBox ID="txtJokeContent" TextMode="MultiLine" Width="200px" Height="80px"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <center>
                                        <p style="text-align: center;">
                                            <asp:Button ID="OkEditButton" runat="server" Text="Save" CausesValidation="false" />
                                            <asp:Button ID="cancelEditButton" runat="server" Text="Cancel" CausesValidation="false" />
                                        </p>
                                    </center>
                                </center>
                            </div>
                        </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="modalPopupEdit" runat="server" TargetControlID="PanelEditContent"
                            PopupControlID="PanelEditContent" BackgroundCssClass="modalBackground" OkControlID="OkEditButton"
                            DropShadow="true" OnOkScript="onOkEdit()" OnCancelScript="hideModalPopupEdit()"
                            CancelControlID="cancelEditButton" PopupDragHandleControlID="PanelEditContentDrag"
                            BehaviorID="modalPopupEdit" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            </td>
            <td valign="top">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
                    <ProgressTemplate>
                        <img id="Img1" src="images/wait.gif" width="40" height="40" runat="server" alt="Loading" /></ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <b>Add New SendJoke:</b>
    <br />
    <table class="GridViewStyle">
        <tr class="HeaderStyle">
            <th align="left">
                JokeViewCode
            </th>
            <td>
                <asp:TextBox Width="200px" ID="txtJokeViewcode2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="HeaderStyle">
            <th align="left">
                Username
            </th>
            <td>
                <asp:DropDownList ID="dropUsers" runat="server" DataSourceID="LinqUsers" DataTextField="UserName"
                    DataValueField="UserName" Width="200px" AppendDataBoundItems="true">
                    <asp:ListItem Text="" Value=""></asp:ListItem>
                </asp:DropDownList>
                <asp:LinqDataSource ID="LinqUsers" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                    OrderBy="UserName" Select="new (UserName, UserId)" TableName="aspnet_Users">
                </asp:LinqDataSource>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="dropUsers" ValidationGroup="add"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr class="HeaderStyle">
            <th align="left">
                FriendName
            </th>
            <td>
                <asp:TextBox Width="200px" ID="txtFriendName2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="HeaderStyle">
            <th align="left">
                FriendEmailID
            </th>
            <td>
                <asp:TextBox Width="200px" ID="txtFriendEmailID2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="HeaderStyle">
            <th align="left">
                JokeContent
            </th>
            <td>
                <asp:TextBox ID="txtJokeContent2" TextMode="MultiLine" Width="200px" Height="80px"
                    runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <asp:LinkButton ID="linkSave" runat="server" OnClick="linkSave_Click" ValidationGroup="add">Save</asp:LinkButton>
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="linkSave"
                    DisplayModalPopupID="ModalPopupExtender1" ConfirmOnFormSubmit="true" />
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="linkSave"
                    PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                <asp:Panel ID="PNL" runat="server" Style="display: none; width: 250px; background-color: White;
                    border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                    Are you sure you want to save this item?
                    <br />
                    <br />
                    <div style="text-align: right;">
                        <asp:Button ID="ButtonOk" runat="server" Text="OK" />
                        <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
