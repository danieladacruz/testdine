﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class SendJokes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void editJoke_Click(object sender, EventArgs e)
        {
            string jokeIDStr = hiddenValue.Value;
            if (!jokeIDStr.Equals(""))
            {
                int jokeID = int.Parse(jokeIDStr);
                DineSelectDataContext dine = new DineSelectDataContext();
                var jokes = from j in dine.DS_SendJokes where j.JokeID == jokeID select j;
                DS_SendJoke joke = jokes.First();

                joke.FriendEmailID = txtFriendEmailID.Text;
                joke.FriendName = txtFriendName.Text;
                joke.JokeContent = txtJokeContent.Text;
                joke.Jokeviewcode = txtJokeViewCode.Text;
                joke.UserName = txtUsername.Text;

                dine.SubmitChanges();
                updatePanel1.Update();
                GridView1.DataBind();
                GridView1.DataBind();
            }
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            DS_SendJoke joke = new DS_SendJoke();

            joke.FriendEmailID = txtFriendEmailID2.Text;
            joke.FriendName = txtFriendName2.Text;
            joke.JokeContent = txtJokeContent2.Text;
            joke.Jokeviewcode = txtJokeViewcode2.Text;
            joke.UserName = dropUsers.SelectedValue;

            dine.DS_SendJokes.InsertOnSubmit(joke);
            dine.SubmitChanges();

            GridView1.DataBind();
            GridView1.DataBind();
            txtJokeViewcode2.Text = "";
            txtJokeContent2.Text = "";
            txtFriendName2.Text = "";
            txtFriendEmailID2.Text = "";
            dropUsers.SelectedValue = "";
            updatePanel1.Update();

            Utilities utilities = new Utilities();
            Panel panel = Page.Master.FindControl("Panel1") as Panel;
            Literal links = panel.FindControl("links") as Literal;
            links.Text = "<ul><li><ul>" + utilities.GetTotalRows() + "</ul></li></ul>";

        }

        protected void edit_Click(object sender, EventArgs e)
        {
            string jokeIDStr = hiddenValue.Value;
            if (!jokeIDStr.Equals(""))
            {
                int jokeID = int.Parse(jokeIDStr);
                DineSelectDataContext dine = new DineSelectDataContext();
                var jokes = from j in dine.DS_SendJokes where j.JokeID == jokeID select j;
                DS_SendJoke joke = jokes.First();

                txtFriendEmailID.Text = joke.FriendEmailID;
                txtFriendName.Text = joke.FriendName;
                txtJokeContent.Text = joke.JokeContent;
                txtJokeViewCode.Text = joke.Jokeviewcode;
                txtUsername.Text = joke.UserName;
            }
            modalPopupEdit.Show();
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                GridView1.AllowPaging = false;
                updatePanel1.Update();
            }
            else
            {
                GridView1.AllowPaging = true;
                GridView1.PageSize = int.Parse(ddlSize.SelectedValue);
                updatePanel1.Update();
            }
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_SendJokes where a.JokeID == key select a;
            try
            {
                dine.DS_SendJokes.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqSendJokes.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("Panel1") as Panel;
                Literal links = panel.FindControl("links") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRows() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Username"))
            {
                LinqSendJokes.Where = "Username.Contains(\"" + txtField.Text + "\")";
                LinqSendJokes.DataBind();
                Session.Add("whereJokes", LinqSendJokes.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
            else if (ddlField.SelectedValue.Equals("JokeContent"))
            {
                LinqSendJokes.Where = "JokeContent.Contains(\"" + txtField.Text + "\")";
                LinqSendJokes.DataBind();
                Session.Add("whereJokes", LinqSendJokes.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
            else
            {
                LinqSendJokes.Where = "JokeID == " + txtField.Text;
                LinqSendJokes.DataBind();
                Session.Add("whereJokes", LinqSendJokes.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            LinqSendJokes.Where = "";
            Session["whereJokes"] = ""; ;
            LinqSendJokes.DataBind();
            GridView1.DataBind();
            GridView1.DataBind();
            updatePanel1.Update();
        }
    }
}
