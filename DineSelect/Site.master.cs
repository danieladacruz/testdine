﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Site : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Utilities utilities = new Utilities();

        if (Context.User.Identity.IsAuthenticated)
        {
            links.Text = "<ul><li><ul>" + utilities.GetTotalRows() + "</ul></li></ul>";
            linksDp.Text = "<ul><li><ul>" + utilities.GetTotalRowsForDependantTables() + "</ul></li></ul>";
        }
        else
        {
            Panel2.Visible = false;
            Panel1.Visible = false;
            PanelDp.Visible = false;
            PanelDp1.Visible = false;
        }

        if (Session["lookup"] != null)
        {
            bool lookup = (bool)Session["lookup"];
            cpeDemo.Collapsed = lookup;
        }

        if (Session["dependant"] != null)
        {
            bool lookup = (bool)Session["dependant"];
            cpeDemoDp.Collapsed = lookup;
        }

        if (utilities.isAdmin(Context.User.Identity.Name))
        {
            users.Visible = true;
        }
        else
        {
            users.Visible = false;
        }
    }

    protected void Image1_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["lookup"] != null)
        {
            bool lookup = (bool)Session["lookup"];
            Session["lookup"] = !lookup;
        }
        else
        {
            Session.Add("lookup", false);
        }
    }

    protected void ImageDp_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["dependant"] != null)
        {
            bool lookup = (bool)Session["dependant"];
            Session["dependant"] = !lookup;
        }
        else
        {
            Session.Add("dependant", false);
        }
    }

}
