﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="SubwayStations.aspx.cs" Inherits="DineSelect.SubwayStations" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" language="javascript">
        function Delete(val) {
            document.getElementById("<%= deleteHidden.ClientID %>").value = val;
            document.getElementById('<%=btnOne.ClientID %>').click();
        }

    </script>

    <asp:Button ID="btnOne" runat="server" Text="One" Style="display: none;" OnClick="btnOne_Click" />
    <asp:Label ID="Label1" Width="100%" runat="server" Text="Subway Stations" CssClass="labelTop"></asp:Label>
    <table>
        <tr>
            <td>
                <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <input type="hidden" id="deleteHidden" runat="server" />
                        <br />
                        <br />
                        <table>
                            <tr>
                                <td>
                                    Rows to Display
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSize" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSize_SelectedIndexChanged">
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                        <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                        <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Search for:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlField" runat="server">
                                        <asp:ListItem Text="Name" Value="Name"></asp:ListItem>
                                        <asp:ListItem Text="ID" Value="ID"></asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:TextBox ID="txtField" runat="server" Width="300px"></asp:TextBox>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="linkSearch" runat="server" OnClick="linkSearch_Click">Search</asp:LinkButton>
                                    &nbsp;
                                    <asp:LinkButton ID="linkClear" runat="server" OnClick="linkClear_Click">Clear</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                        <br />
                        <asp:ListView ID="ListView1" runat="server" DataKeyNames="SubwayStationID" DataSourceID="LinqSubwayStations">
                            <ItemTemplate>
                                <tr class="RowStyle">
                                    <td>
                                        <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                                        <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="DeleteButton"
                                            DisplayModalPopupID="ModalPopupExtender1" />
                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="DeleteButton"
                                            PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                        <asp:Panel ID="PNL" runat="server" Style="display: none; width: 200px; background-color: White;
                                            border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                            Are you sure you want to delete this item?
                                            <br />
                                            <br />
                                            <div style="text-align: right;">
                                                <asp:Button ID="ButtonOk" runat="server" Text="OK" OnClientClick='<%# "Delete(" + Eval("SubwayStationID") + ");"%>' />
                                                <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                            </div>
                                        </asp:Panel>
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl1" runat="server" DataField="SubwayStationID"
                                            Mode="ReadOnly" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl2" runat="server" DataField="Name" Mode="ReadOnly" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl3" runat="server" DataField="DS_SubwayType"
                                            Mode="ReadOnly" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl4" runat="server" DataField="Description" Mode="ReadOnly" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl5" runat="server" DataField="SubwayURL" Mode="ReadOnly" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="AltRowStyle">
                                    <td>
                                        <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                                        <asp:LinkButton ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                                        <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="DeleteButton"
                                            DisplayModalPopupID="ModalPopupExtender1" />
                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="DeleteButton"
                                            PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                        <asp:Panel ID="PNL" runat="server" Style="display: none; width: 200px; background-color: White;
                                            border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                            Are you sure you want to delete this item?
                                            <br />
                                            <br />
                                            <div style="text-align: right;">
                                                <asp:Button ID="ButtonOk" runat="server" Text="OK" OnClientClick='<%# "Delete(" + Eval("SubwayStationID") + ");"%>' />
                                                <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                            </div>
                                        </asp:Panel>
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl6" runat="server" DataField="SubwayStationID"
                                            Mode="ReadOnly" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl7" runat="server" DataField="Name" Mode="ReadOnly" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl8" runat="server" DataField="DS_SubwayType"
                                            Mode="ReadOnly" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl9" runat="server" DataField="Description" Mode="ReadOnly" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl10" runat="server" DataField="SubwayURL" Mode="ReadOnly" />
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <EmptyDataTemplate>
                                <table id="Table1" runat="server" class="GridViewStyle">
                                    <tr>
                                        <td>
                                            No data was returned.
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <LayoutTemplate>
                                <table id="Table2" runat="server" class="GridViewStyle">
                                    <tr id="Tr1" runat="server">
                                        <td id="Td1" runat="server">
                                            <table id="itemPlaceholderContainer" runat="server" border="0" style="">
                                                <tr id="Tr2" runat="server" class="HeaderStyle">
                                                    <th id="Th1" runat="server">
                                                    </th>
                                                    <th id="Th2" runat="server">
                                                        ID
                                                    </th>
                                                    <th id="Th3" runat="server">
                                                        Name
                                                    </th>
                                                    <th id="Th4" runat="server">
                                                        Subway Type
                                                    </th>
                                                    <th id="Th5" runat="server">
                                                        Description
                                                    </th>
                                                    <th id="Th6" runat="server">
                                                        Subway URL
                                                    </th>
                                                </tr>
                                                <tr id="itemPlaceholder" runat="server">
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="Tr3" runat="server">
                                        <td id="Td2" runat="server" style="">
                                            <asp:DataPager ID="DataPager1" runat="server">
                                                <Fields>
                                                    <asp:NextPreviousPagerField ButtonType="Button" ShowNextPageButton="False"
                                                        ShowPreviousPageButton="False" />
                                                    <asp:NumericPagerField />
                                                    <asp:NextPreviousPagerField ButtonType="Button" ShowNextPageButton="False"
                                                        ShowPreviousPageButton="False" />
                                                </Fields>
                                            </asp:DataPager>
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <EditItemTemplate>
                                <tr style="">
                                    <td>
                                        <asp:LinkButton ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                                        <asp:LinkButton ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl11" runat="server" DataField="SubwayStationID"
                                            Mode="ReadOnly" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl12" runat="server" DataField="Name" Mode="Edit" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl17" runat="server" DataField="DS_SubwayType"
                                            Mode="Edit" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl14" runat="server" DataField="Description"
                                            Mode="Edit" />
                                    </td>
                                    <td>
                                        <asp:DynamicControl ID="DynamicControl15" runat="server" DataField="SubwayURL" Mode="Edit" />
                                    </td>
                                </tr>
                            </EditItemTemplate>
                        </asp:ListView>
                        <asp:LinqDataSource ID="LinqSubwayStations" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                            EnableDelete="True" EnableInsert="True" EnableUpdate="True" OrderBy="Name" TableName="DS_SubwayStations">
                        </asp:LinqDataSource>
                        <br />
                        <br />
                        <b>New Subway Station</b>
                        <table class="GridViewStyle">
                            <tr class="HeaderStyle">
                                <th align="left">
                                    Name
                                </th>
                                <td>
                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                        ControlToValidate="txtName" ValidationGroup="add"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr class="HeaderStyle">
                                <th align="left">
                                    Description
                                </th>
                                <td>
                                    <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr class="HeaderStyle">
                                <th align="left">
                                    Subway Type
                                </th>
                                <td>
                                    <asp:DropDownList ID="ddlSubwayType" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                        DataSourceID="LinqSubWayTypes" DataTextField="Name" DataValueField="SubwayTypeID"
                                        Width="150px">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:LinqDataSource ID="LinqSubWayTypes" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                                        OrderBy="Name" Select="new (SubwayTypeID, Name)" TableName="DS_SubwayTypes">
                                    </asp:LinqDataSource>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                        ControlToValidate="ddlSubwayType" ValidationGroup="add"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr class="HeaderStyle">
                                <th align="left">
                                    Subway URL
                                </th>
                                <td>
                                    <asp:TextBox ID="txtURL" runat="server"></asp:TextBox>&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <asp:LinkButton ID="linkSave" runat="server" OnClientClick="return ConfirmOnSave();"
                                        OnClick="linkSave_Click" ValidationGroup="add">Save</asp:LinkButton>
                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="linkSave"
                                        DisplayModalPopupID="ModalPopupExtender1" ConfirmOnFormSubmit="true" />
                                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="linkSave"
                                        PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                                    <asp:Panel ID="PNL" runat="server" Style="display: none; width: 250px; background-color: White;
                                        border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                        Are you sure you want to save this item?
                                        <br />
                                        <br />
                                        <div style="text-align: right;">
                                            <asp:Button ID="ButtonOk" runat="server" Text="OK" />
                                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                        </div>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
                    <ProgressTemplate>
                        <img id="Img1" src="images/wait.gif" width="40" height="40" runat="server" alt="Loading" /></ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>
</asp:Content>
