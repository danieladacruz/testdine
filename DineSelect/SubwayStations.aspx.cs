﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class SubwayStations : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                LinqSubwayStations.Where = Session["whereSubwayStations"] + "";
                LinqSubwayStations.DataBind();
            }
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            DS_SubwayStation atm = new DS_SubwayStation();
            atm.Name = txtName.Text;
            atm.Description = txtDescription.Text;
            atm.SubwayType = byte.Parse(ddlSubwayType.SelectedValue);
            atm.SubwayURL = txtURL.Text;
            
            dine.DS_SubwayStations.InsertOnSubmit(atm);
            dine.SubmitChanges();
            ListView1.DataBind();
            ListView1.DataBind();
            txtDescription.Text = "";
            txtName.Text = "";
            ddlSubwayType.SelectedValue = "";
            updatePanel1.Update();

            Utilities utilities = new Utilities();
            Panel panel = Page.Master.FindControl("Panel1") as Panel;
            Literal links = panel.FindControl("links") as Literal;
            links.Text = "<ul><li><ul>" + utilities.GetTotalRows() + "</ul></li></ul>";
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                DataPager pgr = ListView1.FindControl("DataPager1") as DataPager;
                if (pgr != null)
                {
                    pgr.SetPageProperties(0, pgr.TotalRowCount, false);
                }
                LinqSubwayStations.DataBind();
                LinqSubwayStations.DataBind();
                ListView1.DataBind();
                ListView1.DataBind();
                updatePanel1.Update();
            }
            else
            {
                DataPager pgr = ListView1.FindControl("DataPager1") as DataPager;
                if (pgr != null)// && ListView1.Items.Count != pgr.TotalRowCount)
                {
                    pgr.SetPageProperties(0, int.Parse(ddlSize.SelectedValue), false);
                }
                LinqSubwayStations.DataBind();
                LinqSubwayStations.DataBind();
                ListView1.DataBind();
                ListView1.DataBind();
                updatePanel1.Update();
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Name"))
            {
                LinqSubwayStations.Where = "Name.Contains(\"" + txtField.Text + "\")";
                Session.Add("whereSubwayStations", LinqSubwayStations.Where);
                LinqSubwayStations.DataBind();
                ListView1.DataBind();
                ListView1.DataBind();
                updatePanel1.Update();
            }
            else
            {
                LinqSubwayStations.Where = "SubwayStationID == " + txtField.Text;
                Session.Add("whereSubwayStations", LinqSubwayStations.Where);
                LinqSubwayStations.DataBind();
                ListView1.DataBind();
                ListView1.DataBind();
                updatePanel1.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            LinqSubwayStations.Where = "";
            LinqSubwayStations.DataBind();
            Session["whereSubwayStations"] = "";
            ListView1.DataBind();
            ListView1.DataBind();
            updatePanel1.Update();
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_SubwayStations where a.SubwayStationID == key select a;
            try
            {
                dine.DS_SubwayStations.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqSubwayStations.DataBind();
                ListView1.DataBind();
                ListView1.DataBind();
                updatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("Panel1") as Panel;
                Literal links = panel.FindControl("links") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRows() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }
    }
}
