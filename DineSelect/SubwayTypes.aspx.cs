﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class SubwayTypes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (Page.IsPostBack)
            {
                LinqAtmosphere.Where = Session["whereSubwayType"] + "";
                LinqAtmosphere.DataBind();
            }
        }

        protected void linkSave_Click(object sender, EventArgs e)
        {
            Utilities utilities = new Utilities();
            DineSelectDataContext dine = new DineSelectDataContext();
            var existing = from s in dine.DS_SubwayTypes select s.SubwayTypeID;
            int max = existing.Max() + 1;
            DS_SubwayType atm = new DS_SubwayType();
            atm.SubwayTypeID = max;
            atm.Name = txtName.Text;
            atm.Description = txtDescription.Text;
            dine.DS_SubwayTypes.InsertOnSubmit(atm);
            dine.SubmitChanges();
            GridView1.DataBind();
            GridView1.DataBind();
            txtDescription.Text = "";
            txtName.Text = "";
            updatePanel1.Update();
            Panel panel = Page.Master.FindControl("Panel1") as Panel;
            Literal links = panel.FindControl("links") as Literal;
            links.Text = "<ul><li><ul>" + utilities.GetTotalRows() + "</ul></li></ul>";
        }

        protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSize.SelectedValue.Equals("All"))
            {
                GridView1.AllowPaging = false;
                LinqAtmosphere.DataBind();
                LinqAtmosphere.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
            else
            {
                GridView1.AllowPaging = true;
                GridView1.PageSize = int.Parse(ddlSize.SelectedValue);
                LinqAtmosphere.DataBind();
                LinqAtmosphere.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
        }

        protected void linkSearch_Click(object sender, EventArgs e)
        {
            if (ddlField.SelectedValue.Equals("Name"))
            {
                LinqAtmosphere.Where = "Name.Contains(\"" + txtField.Text + "\")";
                LinqAtmosphere.DataBind();
                Session.Add("whereSubwayType", LinqAtmosphere.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
            else
            {
                LinqAtmosphere.Where = "SubwayTypeID == " + txtField.Text;
                LinqAtmosphere.DataBind();
                Session.Add("whereSubwayType", LinqAtmosphere.Where);
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();
            }
        }

        protected void linkClear_Click(object sender, EventArgs e)
        {
            txtField.Text = "";
            LinqAtmosphere.Where = "";
            LinqAtmosphere.DataBind();
            Session["whereSubwayType"] = "";
            GridView1.DataBind();
            GridView1.DataBind();
            updatePanel1.Update();
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            int key = int.Parse(deleteHidden.Value);
            DineSelectDataContext dine = new DineSelectDataContext();
            var atms = from a in dine.DS_SubwayTypes where a.SubwayTypeID == key select a;
            try
            {
                dine.DS_SubwayTypes.DeleteAllOnSubmit(atms);
                dine.SubmitChanges();
                LinqAtmosphere.DataBind();
                GridView1.DataBind();
                GridView1.DataBind();
                updatePanel1.Update();

                Utilities utilities = new Utilities();
                Panel panel = Page.Master.FindControl("Panel1") as Panel;
                Literal links = panel.FindControl("links") as Literal;
                links.Text = "<ul><li><ul>" + utilities.GetTotalRows() + "</ul></li></ul>";
            }
            catch (Exception)
            {
                lblError.Text = "<img src=\"images/attention.png\" width=\"30px\" height=\"30px\"> ERROR: The selected item has other dependencies and can not be deleted!";
            }
        }
    }
}
