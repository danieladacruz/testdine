﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DineSelect;
using System.Web.Security;

/// <summary>
/// Summary description for Utilities
/// </summary>
public class Utilities
{
    public Utilities()
    {

    }

    public bool isAdmin(string username)
    {
        DineSelectDataContext dine = new DineSelectDataContext();
        var roles = from r in dine.aspnet_UsersInRoles
                    where r.aspnet_User.UserName.Equals(username)
                    select r.aspnet_Role.RoleName;

        return roles.Contains("Admin") ? true : false;
    }

    public string GetTotalRows()
    {
        string links = "";

        DineSelectDataContext dine = new DineSelectDataContext();
        var atm = from a in dine.DS_Atmospheres select a;
        int total1 = atm.Count();
        links += "<li><a href=Atmosphere.aspx>Atmosphere (" + total1 + ")</a></li>";

        var att = from a in dine.DS_AttireTypes select a;
        int total2 = att.Count();
        links += "<li><a href=AttireTypes.aspx>AttireTypes (" + total2 + ")</a></li>";

        var chairs = from a in dine.DS_Chains select a;
        int total3 = chairs.Count();
        links += "<li><a href=Chains.aspx>Chains (" + total3 + ")</a></li>";

        var count = from a in dine.DS_Countries select a;
        int total4 = count.Count();
        links += "<li><a href=Countries.aspx>Countries (" + total4 + ")</a></li>";

        var est = from a in dine.DS_EstablishmentTypes select a;
        int total5 = est.Count();
        links += "<li><a href=EstablishmentTypes.aspx>EstablishmentTypes (" + total5 + ")</a></li>";

        var pay = from a in dine.DS_PaymentMethods select a;
        int total6 = pay.Count();
        links += "<li><a href=PaymentMethod.aspx>PaymentMethods (" + total6 + ")</a></li>";

        var pre = from a in dine.DS_PremiumServices select a;
        int total7 = pre.Count();
        links += "<li><a href=PremiumService.aspx>PremiumServices (" + total7 + ")</a></li>";

        var price = from a in dine.DS_PriceRanges select a;
        int total8 = price.Count();
        links += "<li><a href=PriceRange.aspx>PriceRanges (" + total8 + ")</a></li>";

        var rec = from a in dine.DS_RecipeTypes select a;
        int total9 = rec.Count();
        links += "<li><a href=RecipeTypes.aspx>RecipeTypes (" + total9 + ")</a></li>";

        var send = from a in dine.DS_SendJokes select a;
        int total10 = send.Count();
        links += "<li><a href=SendJokes.aspx>SendJokes (" + total10 + ")</a></li>";

        var spec = from a in dine.DS_SpecialFeatures select a;
        int total11 = spec.Count();
        links += "<li><a href=SpecialFeatures.aspx>SpecialFeatures (" + total11 + ")</a></li>";

        var subtypes = from a in dine.DS_SubwayTypes select a;
        int total13 = subtypes.Count();
        links += "<li><a href=SubwayTypes.aspx>SubwayTypes (" + total13 + ")</a></li>";

        var sub = from a in dine.DS_SubwayStations select a;
        int total12 = sub.Count();
        links += "<li><a href=SubwayStations.aspx>SubwayStations (" + total12 + ")</a></li>";

        return links;
    }

    public string GetTotalRowsForDependantTables()
    {
        string links = "";

        DineSelectDataContext dine = new DineSelectDataContext();

        var prov = from a in dine.DS_Provinces select a;
        int total6 = prov.Count();
        links += "<li><a href=Provinces.aspx>Provinces (" + total6 + ")</a></li>";

        var atm = from a in dine.DS_Cities select a;
        int total1 = atm.Count();
        links += "<li><a href=Cities.aspx>Cities (" + total1 + ")</a></li>";

        var citreg = from a in dine.DS_CityRegions select a;
        int total3 = citreg.Count();
        links += "<li><a href=CityRegions.aspx>Regions (" + total3 + ")</a></li>";

        var citloc = from a in dine.DS_CityLocalities select a;
        int total2 = citloc.Count();
        links += "<li><a href=CityLocalities.aspx>Localities (" + total2 + ")</a></li>";

        var estab = from a in dine.DS_Establishments select a;
        int total5 = estab.Count();
        links += "<li><a href=Establishments.aspx>Establishments (" + total5 + ")</a></li>";

        var establi = from a in dine.DS_EstablishmentLocationIntersections select a;
        int total8 = establi.Count();
        links += "<li><a href=EstablishmentLocationIntersections.aspx>EstabLocationIntersections (" + total8 + ")</a></li>";

        var estabpm = from a in dine.DS_EstablishmentPaymentMethods select a;
        int total9 = estabpm.Count();
        links += "<li><a href=EstablishmentPaymentMethods.aspx>EstabPaymentMethods (" + total9 + ")</a></li>";

        var estabps = from a in dine.DS_EstablishmentPremiumServices select a;
        int total10 = estabps.Count();
        links += "<li><a href=EstablishmentPremiumServices.aspx>EstabPremiumServices (" + total10 + ")</a></li>";

        var estabR = from a in dine.DS_EstablishmentReviews select a;
        int total7 = estabR.Count();
        links += "<li><a href=EstablishmentReviews.aspx>EstabReviews (" + total7 + ")</a></li>";

        var estabsf = from a in dine.DS_EstablishmentSpecialFeatures select a;
        int total11 = estabsf.Count();
        links += "<li><a href=EstablishmentSpecialFeatures.aspx>EstabSpecialFeatures (" + total11 + ")</a></li>";

        var estabst = from a in dine.DS_EstablishmentSubTypes select a;
        int total12 = estabst.Count();
        links += "<li><a href=EstablishmentSubType.aspx>EstabSubType (" + total12 + ")</a></li>";

        var estabImg = from a in dine.DS_Images select a;
        int total14 = estabImg.Count();
        links += "<li><a href=Images.aspx>Images (" + total14 + ")</a></li>";

        var inters = from a in dine.DS_Intersections select a;
        int total4 = inters.Count();
        links += "<li><a href=Intersections.aspx>Intersections (" + total4 + ")</a></li>";

        var estabRec = from a in dine.DS_Recipes select a;
        int total13 = estabRec.Count();
        links += "<li><a href=Recipes.aspx>Recipes (" + total13 + ")</a></li>";

        return links;

    }
}
