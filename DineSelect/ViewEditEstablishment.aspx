﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="ViewEditEstablishment.aspx.cs" Inherits="DineSelect.ViewEditEstablishment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HyperLink NavigateUrl="~/Establishments.aspx" runat="server" ID="linkBack" Text="Back to Establishments"></asp:HyperLink>
    <br />
    <br />
    <asp:Label ID="lblTop" Width="100%" runat="server" CssClass="labelTop"></asp:Label>
    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelView" runat="server">
                <asp:FormView ID="FormView1" runat="server" DataKeyNames="EstablishmentID" DataSourceID="LinqEstablishments">
                    <ItemTemplate>
                        <br />
                        <table class="GridViewStyle" style="padding-left: 15px">
                            <tr>
                                <th class="HeaderStyle">
                                    Name:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="NameDynamicControl" runat="server" DataField="Name" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Establishment Type:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl31" runat="server" DataField="DS_EstablishmentType"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Establishment SubType:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl30" runat="server" DataField="DS_EstablishmentSubType"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Price Range:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl32" runat="server" DataField="DS_PriceRange"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    ShowProfile:
                                </th>
                                <td class="AltRowStyle">
                                    <asp:DynamicControl ID="ShowProfileDynamicControl" runat="server" DataField="ShowProfile"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Is Published:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="IsPublishedDynamicControl" runat="server" DataField="IsPublished"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Is Paying Customer:
                                </th>
                                <td class="AltRowStyle">
                                    <asp:DynamicControl ID="DynamicControl1" runat="server" DataField="IsPayingCustomer"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Is Sponsored Listing:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl2" runat="server" DataField="IsSponsoredListing"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Show Logo:
                                </th>
                                <td class="AltRowStyle">
                                    <asp:DynamicControl ID="DynamicControl3" runat="server" DataField="ShowLogo" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Short Description:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl5" runat="server" DataField="ShortDescription"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Description:
                                </th>
                                <td class="AltRowStyle">
                                    <asp:DynamicControl ID="DynamicControl4" runat="server" DataField="Description" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Address:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl6" runat="server" DataField="Address" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Postal Code:
                                </th>
                                <td class="AltRowStyle">
                                    <asp:DynamicControl ID="DynamicControl7" runat="server" DataField="PostalCode" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    City:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl26" runat="server" DataField="DS_City" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    City Locality:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl27" runat="server" DataField="DS_CityLocality"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    City Region:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl28" runat="server" DataField="DS_CityRegion"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Province:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl33" runat="server" DataField="DS_Province"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Country:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl29" runat="server" DataField="DS_Country" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Phone:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl8" runat="server" DataField="Phone" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Fax:
                                </th>
                                <td class="AltRowStyle">
                                    <asp:DynamicControl ID="DynamicControl9" runat="server" DataField="Fax" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Email:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl10" runat="server" DataField="Email" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Establishment Folder:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl11" runat="server" DataField="EstablishmentFolder"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    URL:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl12" runat="server" DataField="URL" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Latitude:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl13" runat="server" DataField="Latitude" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Longitude:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl14" runat="server" DataField="Longitude" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Rating:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl15" runat="server" DataField="Rating" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Rating Base:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl16" runat="server" DataField="RatingBase" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Average Overall:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl17" runat="server" DataField="AverageOverall"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Average Food:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl18" runat="server" DataField="AverageFood"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Average Service:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl19" runat="server" DataField="AverageService"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    KeyWords:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl20" runat="server" DataField="KeyWords" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Notes:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl21" runat="server" DataField="Notes" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    User:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl22" runat="server" DataField="DS_UserRegistration"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Atmosphere:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl23" runat="server" DataField="DS_Atmosphere"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Attire Type:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl24" runat="server" DataField="DS_AttireType"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Chain:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl25" runat="server" DataField="DS_Chain" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Subway Station:
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl34" runat="server" DataField="DS_SubwayStation"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:FormView>
                <asp:LinqDataSource ID="LinqEstablishments" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                    TableName="DS_Establishments" Where="EstablishmentID == @EstablishmentID">
                    <WhereParameters>
                        <asp:QueryStringParameter Name="EstablishmentID" QueryStringField="id" Type="Int32" />
                    </WhereParameters>
                </asp:LinqDataSource>
            </asp:Panel>
            <asp:Panel ID="panelEdit" runat="server">
                <table class="GridViewStyle" style="padding-left: 15px">
                    <tr>
                        <th class="HeaderStyle">
                            Name:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtName" runat="server" Width="220px" />
                            <asp:RequiredFieldValidator ValidationGroup="save" ControlToValidate="txtName" ID="RequiredFieldValidator2"
                                runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Establishment Type:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlEstablishmentType" runat="server" Width="220px" />
                            <asp:RequiredFieldValidator ValidationGroup="save" ControlToValidate="ddlEstablishmentType"
                                ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Establishment SubType:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlEstablishmentSubType" runat="server" Width="220px" />
                            <asp:RequiredFieldValidator ValidationGroup="save" ControlToValidate="ddlEstablishmentSubType"
                                ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Price Range:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlPriceRange" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Show Profile:
                        </th>
                        <td class="AltRowStyle">
                            <asp:CheckBox ID="checkShowProfile" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Is Published:
                        </th>
                        <td class="RowStyle">
                            <asp:CheckBox ID="checkIsPublished" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Is Paying Customer:
                        </th>
                        <td class="AltRowStyle">
                            <asp:CheckBox ID="checkIsPayingCustomer" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Is Sponsored Listing:
                        </th>
                        <td class="RowStyle">
                            <asp:CheckBox ID="checkIsSponsoredListing" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Show Logo:
                        </th>
                        <td class="AltRowStyle">
                            <asp:CheckBox ID="checkShowLogo" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Short Description:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtShortDescription" runat="server" Width="220px" />
                            <asp:RequiredFieldValidator ValidationGroup="save" ControlToValidate="txtShortDescription"
                                ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Description:
                        </th>
                        <td class="AltRowStyle">
                            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="50px"
                                Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Address:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtAddress" runat="server" Width="220px" />
                            <asp:RequiredFieldValidator ValidationGroup="save" ControlToValidate="txtAddress"
                                ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Postal Code:
                        </th>
                        <td class="AltRowStyle">
                            <asp:TextBox ID="txtPostalCode" runat="server" Width="220px" />
                            <asp:RequiredFieldValidator ValidationGroup="save" ControlToValidate="txtPostalCode"
                                ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Country:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlCountries" runat="server" OnSelectedIndexChanged="ddlCountries_SelectedIndexChanged"
                                AutoPostBack="True" Width="220px" />
                            <asp:RequiredFieldValidator ValidationGroup="save" ControlToValidate="ddlCountries"
                                ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Province:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlProvinces" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlProvinces_SelectedIndexChanged"
                                Width="220px" />
                            <asp:RequiredFieldValidator ValidationGroup="save" ControlToValidate="ddlProvinces"
                                ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            City:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlCities" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCities_SelectedIndexChanged"
                                Width="220px" />
                            <asp:RequiredFieldValidator ValidationGroup="save" ControlToValidate="ddlCities"
                                ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            City Region:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlRegions" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRegions_SelectedIndexChanged"
                                Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            City Locality:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlLocalities" runat="server" AutoPostBack="True" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Phone:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtPhone" runat="server" Width="220px" />
                            <asp:RequiredFieldValidator ValidationGroup="save" ControlToValidate="txtPhone" ID="RequiredFieldValidator12"
                                runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Fax:
                        </th>
                        <td class="AltRowStyle">
                            <asp:TextBox ID="txtFax" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Email:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtEmail" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Establishment Folder:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtFolder" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            URL:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtUrl" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Latitude:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtLatitude" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Longitude:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtLongitude" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Rating:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtRating" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Rating Base:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtRatingBase" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Average Overall:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtAverageOverall" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Average Food:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtAverageFood" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Average Service:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtAverageService" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            KeyWords:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtKeywords" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Notes:
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtnotes" runat="server" Height="50px" Width="220px" TextMode="MultiLine" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            User:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlUser" runat="server" Width="220px" AppendDataBoundItems="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Atmosphere:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlAtmosphere" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Attire Type:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlAttireType" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Chain:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlChain" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Subway Station:
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlSubwayStation" runat="server" Width="220px" />
                        </td>
                    </tr>
                </table>
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:LinkButton ID="linkUpdate" runat="server" ValidationGroup="save" CausesValidation="true"
                                OnClick="linkUpdate_Click">Update</asp:LinkButton>
                            <asp:LinkButton ID="linkInsert" runat="server" ValidationGroup="save" CausesValidation="true"
                                OnClick="linkInsert_Click">Insert</asp:LinkButton>
                                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="linkInsert"
                        DisplayModalPopupID="ModalPopupExtender1" />
                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="linkInsert"
                        PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                    <asp:Panel ID="PNL" runat="server" Style="display: none; width: 200px; background-color: White;
                        border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                        Are you sure you want to save this item?
                        <br />
                        <br />
                        <div style="text-align: right;">
                            <asp:Button ID="ButtonOk" runat="server" Text="OK" />
                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                        </div>
                    </asp:Panel>
                        </td>
                        <td>
                            <asp:LinkButton ID="linkCancel" runat="server" OnClick="linkCancel_Click">Cancel</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
