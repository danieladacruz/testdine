﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DineSelect
{
    public partial class ViewEditEstablishment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            string mode = Request.QueryString["mode"];

            if (mode == null && id == null)
            {
                Response.Redirect("Establishments.aspx");
            }
            else
            {
                DineSelectDataContext dine = new DineSelectDataContext();

                if (!Page.IsPostBack)
                {
                    var countries = from c in dine.DS_Countries
                                    orderby c.Name
                                    select c;
                    ddlCountries.DataSource = countries;
                    ddlCountries.DataTextField = "Name";
                    ddlCountries.DataValueField = "CountryID";
                    if (countries.Count() > 1)
                    {
                        ddlCountries.Items.Insert(0, new ListItem("", ""));
                    }
                    else if (countries.Count() == 1)
                    {
                        BindProvincesForCountry(countries.First().CountryID);
                    }
                    ddlCountries.DataBind();

                    BindEstablishmentTypes();
                    BindEstablishmentSubTypes();
                    BindPriceRanges();
                    BindAtmosphere();
                    BindAttireType();
                    BindChain();
                    BindSubwayStation();
                    BindUsers();

                    if (mode.Equals("edit"))
                    {
                        panelEdit.Visible = true;
                        PanelView.Visible = false;
                        linkInsert.Visible = false;
                        linkUpdate.Visible = true;

                        var establ = from est in dine.DS_Establishments
                                     where est.EstablishmentID == int.Parse(id)
                                     select est;
                        if (establ.Count() > 0)
                        {
                            lblTop.Text = "Edition of the establishment \"<i>" + establ.First().Name + "</i>\":";
                            BindDataEstablishment(establ.First());
                        }
                    }
                    else if (mode.Equals("insert"))
                    {
                        panelEdit.Visible = true;
                        PanelView.Visible = false;
                        linkInsert.Visible = true;
                        linkUpdate.Visible = false;

                        lblTop.Text = "Creation of new Establishment";
                    }
                    else if (mode.Equals("view"))
                    {
                        panelEdit.Visible = false;
                        PanelView.Visible = true;
                        var establ = from est in dine.DS_Establishments
                                     where est.EstablishmentID == int.Parse(id)
                                     select est;
                        if (establ.Count() > 0)
                        {
                            lblTop.Text = "Details of the establishment \"<i>" + establ.First().Name + "</i>\":";
                            BindDataEstablishment(establ.First());
                        }
                    }
                }

                

                
            }


        }

        private void BindDataEstablishment(DS_Establishment establishment)
        {
            txtName.Text = establishment.Name;
            ddlEstablishmentType.SelectedValue = establishment.EstablishmentTypeID + "";
            ddlEstablishmentSubType.SelectedValue = establishment.EstablishmentSubTypeID + "";
            ddlPriceRange.SelectedValue = establishment.PriceRangeID + "";
            checkShowProfile.Checked = establishment.ShowProfile;
            checkIsPublished.Checked = establishment.IsPublished;
            checkIsPayingCustomer.Checked = establishment.IsPayingCustomer;
            checkIsSponsoredListing.Checked = establishment.IsSponsoredListing;
            checkShowLogo.Checked = establishment.ShowLogo;
            txtShortDescription.Text = establishment.ShortDescription;
            txtDescription.Text = establishment.Description;
            txtAddress.Text = establishment.Address;
            txtPostalCode.Text = establishment.PostalCode;
            ddlCountries.SelectedValue = establishment.CountryID + "";
            ddlProvinces.SelectedValue = establishment.ProvinceID + "";
            ddlCities.SelectedValue = establishment.CityID + "";
            ddlRegions.SelectedValue = establishment.CityRegionID + "";
            ddlLocalities.SelectedValue = establishment.CityLocalityID + "";
            txtPhone.Text = establishment.Phone;
            txtFax.Text = establishment.Fax;
            txtEmail.Text = establishment.Email;
            txtFolder.Text = establishment.EstablishmentFolder;
            txtUrl.Text = establishment.URL;
            txtLatitude.Text = establishment.Latitude.HasValue ? establishment.Latitude.Value + "" : "";
            txtLongitude.Text = establishment.Longitude.HasValue ? establishment.Longitude.Value + "" : "";
            txtRating.Text = establishment.Rating.HasValue ? establishment.Rating.Value + "" : "";
            txtRatingBase.Text = establishment.RatingBase.HasValue ? establishment.RatingBase.Value + "" : "";
            txtAverageOverall.Text = establishment.AverageOverall.HasValue ? establishment.AverageOverall.Value + "" : "";
            txtAverageFood.Text = establishment.AverageFood.HasValue ? establishment.AverageFood.Value + "" : "";
            txtAverageService.Text = establishment.AverageService.HasValue ? establishment.AverageService.Value + "" : "";
            txtKeywords.Text = establishment.KeyWords;
            txtnotes.Text = establishment.Notes;
            ddlUser.SelectedValue = establishment.UserId.HasValue ? establishment.UserId.Value.ToString() : "";
            ddlAtmosphere.SelectedValue = establishment.AtmosphereTypeID + "";
            ddlAttireType.SelectedValue = establishment.AttireTypeID + "";
            ddlChain.SelectedValue = establishment.ChainID + "";
            ddlSubwayStation.SelectedValue = establishment.SubwayStationID + "";
        }

        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0"))
            {
                BindProvincesForCountry(int.Parse(ddlCountries.SelectedValue));
            }
        }


        protected void ddlProvinces_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0"))
            {
                BindCitiesForProvince(int.Parse(ddlProvinces.SelectedValue));
            }
        }



        protected void ddlCities_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0") &&
                !ddlCities.SelectedValue.Equals("") && !ddlCities.SelectedValue.Equals("0"))
            {
                BindRegionsForCity(int.Parse(ddlCities.SelectedValue));
            }
        }

        protected void ddlRegions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!ddlCountries.SelectedValue.Equals("") && !ddlCountries.SelectedValue.Equals("0") &&
                !ddlProvinces.SelectedValue.Equals("") && !ddlProvinces.SelectedValue.Equals("0") &&
                !ddlCities.SelectedValue.Equals("") && !ddlCities.SelectedValue.Equals("0") &&
                !ddlRegions.SelectedValue.Equals("") && !ddlRegions.SelectedValue.Equals("0"))
            {
                BindLocalitiesForRegion(int.Parse(ddlRegions.SelectedValue));
            }
        }

        private void BindUsers()
        {
            ddlUser.Items.Clear();
             DineSelectDataContext dine = new DineSelectDataContext();
             var users = from c in dine.DS_UserRegistrations
                         orderby c.FirstName, c.LastName
                         select new { name = c.FirstName + " " + c.LastName, userid = c.UserId };
             ddlUser.DataSource = users;
             ddlUser.DataTextField = "name";
             ddlUser.DataValueField = "userid";
             if (users.Count() > 1)
             {
                 ddlUser.Items.Insert(0, new ListItem("", ""));
             }
             ddlUser.DataBind();
        }

        private void BindCitiesForProvince(int province)
        {
            ddlCities.Items.Clear();
            ddlRegions.Items.Clear();
            ddlLocalities.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var cities = from c in dine.DS_Cities
                         where c.ProvinceID == province
                         select c;

            ddlCities.DataSource = cities;
            ddlCities.DataTextField = "Name";
            ddlCities.DataValueField = "provinceID";
            ddlCities.AppendDataBoundItems = true;
            if (cities.Count() > 1)
            {
                ddlCities.Items.Insert(0, new ListItem("", ""));
            }
            else if (cities.Count() == 1)
            {
                BindRegionsForCity(cities.First().CityID);
            }
            ddlCities.DataBind();
        }

        private void BindRegionsForCity(int city)
        {
            ddlRegions.Items.Clear();
            ddlLocalities.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var regions = from c in dine.DS_CityRegions
                          where c.CityID == city
                          select c;

            ddlRegions.DataSource = regions;
            ddlRegions.DataTextField = "Name";
            ddlRegions.DataValueField = "CityRegionID";
            ddlRegions.AppendDataBoundItems = true;
            if (regions.Count() > 1)
            {
                ddlRegions.Items.Insert(0, new ListItem("", ""));
            }
            else if (regions.Count() == 1)
            {
                BindLocalitiesForRegion(regions.First().CityRegionID);
            }
            ddlRegions.DataBind();
        }

        private void BindLocalitiesForRegion(int region)
        {
            ddlLocalities.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var intersections = from c in dine.DS_CityLocalities
                                where c.CityRegionID == region
                                select c;

            ddlLocalities.DataSource = intersections;
            ddlLocalities.DataTextField = "Name";
            ddlLocalities.DataValueField = "CityLocalityID";
            ddlLocalities.AppendDataBoundItems = true;
            if (intersections.Count() > 1)
            {
                ddlLocalities.Items.Insert(0, new ListItem("", ""));
            }

            ddlLocalities.DataBind();
        }

        private void BindProvincesForCountry(int country)
        {
            ddlProvinces.Items.Clear();
            ddlCities.Items.Clear();
            ddlRegions.Items.Clear();
            ddlLocalities.Items.Clear();
            DineSelectDataContext dine = new DineSelectDataContext();
            var provinces = from p in dine.DS_Provinces
                            where p.CountryID == country
                            select p;
            ddlProvinces.DataSource = provinces;
            ddlProvinces.DataTextField = "Name";
            ddlProvinces.DataValueField = "provinceID";
            ddlProvinces.AppendDataBoundItems = true;
            if (provinces.Count() > 1)
            {
                ddlProvinces.Items.Insert(0, new ListItem("", ""));
            }
            else if (provinces.Count() == 1)
            {
                BindCitiesForProvince(provinces.First().ProvinceID);
            }
            ddlProvinces.DataBind();
        }

        private void BindEstablishmentTypes()
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var types = from p in dine.DS_EstablishmentTypes
                        orderby p.Name
                        select p;
            ddlEstablishmentType.DataSource = types;
            ddlEstablishmentType.DataTextField = "Name";
            ddlEstablishmentType.DataValueField = "EstablishmentTypeID";
            ddlEstablishmentType.AppendDataBoundItems = true;
            if (types.Count() > 1)
            {
                ddlEstablishmentType.Items.Insert(0, new ListItem("", ""));
            }
            ddlEstablishmentType.DataBind();
        }

        private void BindEstablishmentSubTypes()
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var types = from p in dine.DS_EstablishmentSubTypes
                        orderby p.Name
                        select p;
            ddlEstablishmentSubType.DataSource = types;
            ddlEstablishmentSubType.DataTextField = "Name";
            ddlEstablishmentSubType.DataValueField = "EstablishmentSubTypeID";
            ddlEstablishmentSubType.AppendDataBoundItems = true;
            if (types.Count() > 1)
            {
                ddlEstablishmentSubType.Items.Insert(0, new ListItem("", ""));
            }
            ddlEstablishmentSubType.DataBind();
        }


        private void BindPriceRanges()
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var types = from p in dine.DS_PriceRanges
                        orderby p.Name
                        select p;
            ddlPriceRange.DataSource = types;
            ddlPriceRange.DataTextField = "Name";
            ddlPriceRange.DataValueField = "PriceRangeID";
            ddlPriceRange.AppendDataBoundItems = true;
            if (types.Count() > 1)
            {
                ddlPriceRange.Items.Insert(0, new ListItem("", ""));
            }
            ddlPriceRange.DataBind();
        }

        private void BindAtmosphere()
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var types = from p in dine.DS_Atmospheres
                        orderby p.Name
                        select p;
            ddlAtmosphere.DataSource = types;
            ddlAtmosphere.DataTextField = "Name";
            ddlAtmosphere.DataValueField = "AtmosphereID";
            ddlAtmosphere.AppendDataBoundItems = true;
            if (types.Count() > 1)
            {
                ddlAtmosphere.Items.Insert(0, new ListItem("", ""));
            }
            ddlAtmosphere.DataBind();
        }

        private void BindAttireType()
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var types = from p in dine.DS_AttireTypes
                        orderby p.Name
                        select p;
            ddlAttireType.DataSource = types;
            ddlAttireType.DataTextField = "Name";
            ddlAttireType.DataValueField = "AttireTypeID";
            ddlAttireType.AppendDataBoundItems = true;
            if (types.Count() > 1)
            {
                ddlAttireType.Items.Insert(0, new ListItem("", ""));
            }
            ddlAttireType.DataBind();
        }

        private void BindChain()
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var types = from p in dine.DS_Chains
                        orderby p.Name
                        select p;
            ddlChain.DataSource = types;
            ddlChain.DataTextField = "Name";
            ddlChain.DataValueField = "ChainID";
            ddlChain.AppendDataBoundItems = true;
            if (types.Count() > 1)
            {
                ddlChain.Items.Insert(0, new ListItem("", ""));
            }
            ddlChain.DataBind();
        }

        private void BindSubwayStation()
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var types = from p in dine.DS_SubwayStations
                        orderby p.Name
                        select p;
            ddlSubwayStation.DataSource = types;
            ddlSubwayStation.DataTextField = "Name";
            ddlSubwayStation.DataValueField = "SubwayStationID";
            ddlSubwayStation.AppendDataBoundItems = true;
            if (types.Count() > 1)
            {
                ddlSubwayStation.Items.Insert(0, new ListItem("", ""));
            }
            ddlSubwayStation.DataBind();
        }

        protected void linkUpdate_Click(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            DineSelectDataContext dine = new DineSelectDataContext();
            var establ = from esta in dine.DS_Establishments
                         where esta.EstablishmentID == int.Parse(id)
                         select esta;
            DS_Establishment est = establ.First();

            est.Address = txtAddress.Text;
            if (!ddlAtmosphere.SelectedValue.Equals(""))
            {
                est.AtmosphereTypeID = int.Parse(ddlAtmosphere.SelectedValue);
            }
            if (!ddlAttireType.SelectedValue.Equals(""))
            {
                est.AttireTypeID = int.Parse(ddlAttireType.SelectedValue);
            }
            if (!txtAverageFood.Text.Equals(""))
            {
                est.AverageFood = double.Parse(txtAverageFood.Text);
            }
            if (!txtAverageOverall.Text.Equals(""))
            {
                est.AverageOverall = double.Parse(txtAverageOverall.Text);
            }
            if (!txtAverageService.Text.Equals(""))
            {
                est.AverageService = int.Parse(txtAverageService.Text);
            }
            if (!ddlChain.SelectedValue.Equals(""))
            {
                est.ChainID = int.Parse(ddlChain.SelectedValue);
            }
            est.CityID = int.Parse(ddlCities.SelectedValue);
            if (!ddlLocalities.SelectedValue.Equals(""))
            {
                est.CityLocalityID = int.Parse(ddlLocalities.SelectedValue);
            }
            if (!ddlRegions.SelectedValue.Equals(""))
            {
                est.CityRegionID = int.Parse(ddlRegions.SelectedValue);
            }
            est.CountryID = int.Parse(ddlCountries.SelectedValue);
            est.Description = txtDescription.Text;
            est.Email = txtEmail.Text;
            est.EstablishmentFolder = txtFolder.Text;
            est.EstablishmentSubTypeID = int.Parse(ddlEstablishmentSubType.SelectedValue);
            est.EstablishmentTypeID = int.Parse(ddlEstablishmentType.SelectedValue);
            est.Fax = txtFax.Text;
            est.IsPayingCustomer = checkIsPayingCustomer.Checked;
            est.IsPublished = checkIsPublished.Checked;
            est.IsSponsoredListing = checkIsSponsoredListing.Checked;
            est.KeyWords = txtKeywords.Text;
            if (!txtLongitude.Text.Equals(""))
            {
                est.Longitude = double.Parse(txtLongitude.Text);
            }
            if (!txtLatitude.Text.Equals(""))
            {
                est.Latitude = double.Parse(txtLatitude.Text);
            }
            est.Name = txtName.Text;
            est.Notes = txtnotes.Text;
            est.Phone = txtPhone.Text;
            est.PostalCode = txtPostalCode.Text;
            if (!ddlPriceRange.SelectedValue.Equals(""))
            {
                est.PriceRangeID = int.Parse(ddlPriceRange.SelectedValue);
            }
            est.ProvinceID = int.Parse(ddlProvinces.SelectedValue);
            if (!txtRating.Text.Equals(""))
            {
                est.Rating = double.Parse(txtRating.Text);
            }
            if (!txtRatingBase.Text.Equals(""))
            {
                est.RatingBase = int.Parse(txtRatingBase.Text);
            }
            est.ShortDescription = txtShortDescription.Text;
            est.ShowLogo = checkShowLogo.Checked;
            est.ShowProfile = checkShowProfile.Checked;
            if (!ddlSubwayStation.SelectedValue.Equals(""))
            {
                est.SubwayStationID = int.Parse(ddlSubwayStation.SelectedValue);
            }
            est.URL = txtUrl.Text;
            if (!ddlUser.SelectedValue.Equals(""))
            {
                est.UserId = new Guid(ddlUser.SelectedValue);
            }
            dine.SubmitChanges();
            Response.Redirect("ViewEditEstablishment.aspx?id=" + id + "&mode=view");
        }

        protected void linkCancel_Click(object sender, EventArgs e)
        {
            string mode = Request.QueryString["mode"];
            string id = Request.QueryString["id"];
            if (mode != null && mode.Equals("edit"))
            {
                Response.Redirect("ViewEditEstablishmentaspx?id=" + id + "&mode=view");
            }
            else
            {
                Response.Redirect("Establishments.aspx");
            }
        }

        protected void linkInsert_Click(object sender, EventArgs e)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            DS_Establishment est = new DS_Establishment();

            est.Address = txtAddress.Text;
            if (!ddlAtmosphere.SelectedValue.Equals(""))
            {
                est.AtmosphereTypeID = int.Parse(ddlAtmosphere.SelectedValue);
            }
            if (!ddlAttireType.SelectedValue.Equals(""))
            {
                est.AttireTypeID = int.Parse(ddlAttireType.SelectedValue);
            }
            if (!txtAverageFood.Text.Equals(""))
            {
                est.AverageFood = double.Parse(txtAverageFood.Text);
            }
            if (!txtAverageOverall.Text.Equals(""))
            {
                est.AverageOverall = double.Parse(txtAverageOverall.Text);
            }
            if (!txtAverageService.Text.Equals(""))
            {
                est.AverageService = int.Parse(txtAverageService.Text);
            }
            if (!ddlChain.SelectedValue.Equals(""))
            {
                est.ChainID = int.Parse(ddlChain.SelectedValue);
            }
            est.CityID = int.Parse(ddlCities.SelectedValue);
            if (!ddlLocalities.SelectedValue.Equals(""))
            {
                est.CityLocalityID = int.Parse(ddlLocalities.SelectedValue);
            }
            if (!ddlRegions.SelectedValue.Equals(""))
            {
                est.CityRegionID = int.Parse(ddlRegions.SelectedValue);
            }
            est.CountryID = int.Parse(ddlCountries.SelectedValue);
            est.Description = txtDescription.Text;
            est.Email = txtEmail.Text;
            est.EstablishmentFolder = txtFolder.Text;
            est.EstablishmentSubTypeID = int.Parse(ddlEstablishmentSubType.SelectedValue);
            est.EstablishmentTypeID = int.Parse(ddlEstablishmentType.SelectedValue);
            est.Fax = txtFax.Text;
            est.IsPayingCustomer = checkIsPayingCustomer.Checked;
            est.IsPublished = checkIsPublished.Checked;
            est.IsSponsoredListing = checkIsSponsoredListing.Checked;
            est.KeyWords = txtKeywords.Text;
            if (!txtLongitude.Text.Equals(""))
            {
                est.Longitude = double.Parse(txtLongitude.Text);
            }
            if (!txtLatitude.Text.Equals(""))
            {
                est.Latitude = double.Parse(txtLatitude.Text);
            }
            est.Name = txtName.Text;
            est.Notes = txtnotes.Text;
            est.Phone = txtPhone.Text;
            est.PostalCode = txtPostalCode.Text;
            if (!ddlPriceRange.SelectedValue.Equals(""))
            {
                est.PriceRangeID = int.Parse(ddlPriceRange.SelectedValue);
            }
            est.ProvinceID = int.Parse(ddlProvinces.SelectedValue);
            if (!txtRating.Text.Equals(""))
            {
                est.Rating = double.Parse(txtRating.Text);
            }
            if (!txtRatingBase.Text.Equals(""))
            {
                est.RatingBase = int.Parse(txtRatingBase.Text);
            }
            est.ShortDescription = txtShortDescription.Text;
            est.ShowLogo = checkShowLogo.Checked;
            est.ShowProfile = checkShowProfile.Checked;
            if (!ddlSubwayStation.SelectedValue.Equals(""))
            {
                est.SubwayStationID = int.Parse(ddlSubwayStation.SelectedValue);
            }
            est.URL = txtUrl.Text;
            if (!ddlUser.SelectedValue.Equals(""))
            {
                est.UserId = new Guid(ddlUser.SelectedValue);
            }
            dine.DS_Establishments.InsertOnSubmit(est);
            dine.SubmitChanges();
            Response.Redirect("Establishments.aspx");
        }
    }
}
