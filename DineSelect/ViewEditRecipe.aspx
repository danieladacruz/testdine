﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="ViewEditRecipe.aspx.cs" Inherits="DineSelect.ViewEditRecipe" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HyperLink NavigateUrl="~/Recipes.aspx" runat="server" ID="linkBack" Text="Back to Recipes"></asp:HyperLink>
    <br />
    <br />
    <asp:Label ID="lblTop" Width="100%" runat="server" CssClass="labelTop"></asp:Label>
    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelView" runat="server">
                <asp:FormView ID="FormView1" runat="server" DataKeyNames="RecipeID" DataSourceID="LinqRecipes">
                    <ItemTemplate>
                        <br />
                        <table class="GridViewStyle" style="padding-left: 15px">
                            <tr>
                                <th class="HeaderStyle">
                                    Recipe Type
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl4" runat="server" DataField="DS_RecipeType"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Name
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="NameDynamicControl" runat="server" DataField="Name" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Author
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl31" runat="server" DataField="Author" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Short Description
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl30" runat="server" DataField="ShortDescription"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Servings
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl32" runat="server" DataField="Servings" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Preparation Time (minutes)
                                </th>
                                <td class="AltRowStyle">
                                    <asp:DynamicControl ID="ShowProfileDynamicControl" runat="server" DataField="PreparationTimeMinutes"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Ready In (minutes)
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="IsPublishedDynamicControl" runat="server" DataField="ReadyInTimeMinutes"
                                        Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Ingredients
                                </th>
                                <td class="AltRowStyle">
                                    <asp:DynamicControl ID="DynamicControl1" runat="server" DataField="Ingredients" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Directions
                                </th>
                                <td class="RowStyle">
                                    <asp:DynamicControl ID="DynamicControl2" runat="server" DataField="Directions" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Notes
                                </th>
                                <td class="AltRowStyle">
                                    <asp:DynamicControl ID="DynamicControl3" runat="server" DataField="Notes" Mode="ReadOnly" />
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Source Link
                                </th>
                                <td class="RowStyle">
                                    <a href='<%# Eval("SourceLink") %>' target="_blank">
                                        <%# Eval("SourceLink")%></a>
                                </td>
                            </tr>
                            <tr>
                                <th class="HeaderStyle">
                                    Picture
                                </th>
                                <td class="AltRowStyle">
                                    <img src='<%# Eval("Picture") != null ? "Thumbnail.ashx?p=" + Eval("Picture") : ""%>'
                                        alt='<%# Eval("Picture") %>' />
                                    <br />
                                    <a href='<%# Eval("Picture") != null ? ("http://www.dineselect.com/" + Eval("Picture")) : "" %>'>
                                        <%# Eval("Picture") != null ? ("http://www.dineselect.com/" + Eval("Picture")) : "" %>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:FormView>
                <asp:LinqDataSource ID="LinqRecipes" runat="server" ContextTypeName="DineSelect.DineSelectDataContext"
                    TableName="DS_Recipes" Where="RecipeID == @RecipeID">
                    <WhereParameters>
                        <asp:QueryStringParameter Name="RecipeID" QueryStringField="id" Type="Int32" />
                    </WhereParameters>
                </asp:LinqDataSource>
            </asp:Panel>
            <asp:Panel ID="panelEdit" runat="server">
                <table class="GridViewStyle" style="padding-left: 15px">
                    <tr>
                        <th class="HeaderStyle">
                            Recipe Type
                        </th>
                        <td class="RowStyle">
                            <asp:DropDownList ID="ddlRecipeTypes" runat="server" Width="220px" />
                            <asp:RequiredFieldValidator ValidationGroup="save" ControlToValidate="ddlRecipeTypes"
                                ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Name
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtName" runat="server" Width="220px" />
                            <asp:RequiredFieldValidator ValidationGroup="save" ControlToValidate="txtName" ID="RequiredFieldValidator2"
                                runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Auhor
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtAuthor" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Short Description
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtShortDescription" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Servings
                        </th>
                        <td class="AltRowStyle">
                            <asp:TextBox ID="txtServings" runat="server" TextMode="MultiLine" Height="50px" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Preparation Time (minutes)
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtPreparation" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Ready In (minutes)
                        </th>
                        <td class="AltRowStyle">
                            <asp:TextBox ID="txtReady" runat="server" Width="220px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Ingredients
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtIngredients" runat="server" Width="420px" Height="150px" TextMode="MultiLine" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Directions
                        </th>
                        <td class="AltRowStyle">
                            <asp:TextBox ID="txtDirections" runat="server" Width="420px" Height="150px" TextMode="MultiLine" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Notes
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtNotes" runat="server" Width="420px" Height="150px" TextMode="MultiLine" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Source Link
                        </th>
                        <td class="RowStyle">
                            <asp:TextBox ID="txtLink" runat="server" Width="420px" />
                        </td>
                    </tr>
                    <tr>
                        <th class="HeaderStyle">
                            Picture
                        </th>
                        <td class="RowStyle">
                            <asp:Panel ID="panelPic" runat="server">
                            </asp:Panel>
                            <br />
                            <asp:FileUpload ID="fileUpload" runat="server" Width="100%" />
                        </td>
                    </tr>
                </table>
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:LinkButton ID="linkUpdate" runat="server" ValidationGroup="save" CausesValidation="true"
                                OnClick="linkUpdate_Click">Update</asp:LinkButton>
                            <asp:LinkButton ID="linkInsert" runat="server" ValidationGroup="save" CausesValidation="true"
                                OnClick="linkInsert_Click">Insert</asp:LinkButton>
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" TargetControlID="linkInsert"
                                DisplayModalPopupID="ModalPopupExtender1" />
                            <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="linkInsert"
                                PopupControlID="PNL" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="modalBackground" />
                            <asp:Panel ID="PNL" runat="server" Style="display: none; width: 200px; background-color: White;
                                border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                                Are you sure you want to save this item?
                                <br />
                                <br />
                                <div style="text-align: right;">
                                    <asp:Button ID="ButtonOk" runat="server" Text="OK" />
                                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" />
                                </div>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:LinkButton ID="linkCancel" runat="server" OnClick="linkCancel_Click">Cancel</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="linkUpdate" />
            <asp:PostBackTrigger ControlID="linkInsert" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
