﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace DineSelect
{
    public partial class ViewEditRecipe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            string mode = Request.QueryString["mode"];

            if (mode == null && id == null)
            {
                Response.Redirect("Recipes.aspx");
            }
            else
            {
                DineSelectDataContext dine = new DineSelectDataContext();

                if (!Page.IsPostBack)
                {
                    BindRecipeTypes();
                    if (mode.Equals("edit"))
                    {
                        panelEdit.Visible = true;
                        PanelView.Visible = false;
                        linkInsert.Visible = false;
                        linkUpdate.Visible = true;

                        var establ = from est in dine.DS_Recipes
                                     where est.RecipeID == int.Parse(id)
                                     select est;
                        if (establ.Count() > 0)
                        {
                            lblTop.Text = "Edition of the recipe \"<i>" + establ.First().Name + "</i>\":";
                            BindDataRecipe(establ.First());
                        }
                    }
                    else if (mode.Equals("insert"))
                    {
                        panelEdit.Visible = true;
                        PanelView.Visible = false;
                        linkInsert.Visible = true;
                        linkUpdate.Visible = false;

                        lblTop.Text = "Creation of new Recipe";
                    }
                    else if (mode.Equals("view"))
                    {
                        panelEdit.Visible = false;
                        PanelView.Visible = true;
                        var establ = from est in dine.DS_Recipes
                                     where est.RecipeID == int.Parse(id)
                                     select est;
                        if (establ.Count() > 0)
                        {
                            lblTop.Text = "Details of the recipe \"<i>" + establ.First().Name + "</i>\":";
                            BindDataRecipe(establ.First());
                        }
                    }
                }
            }
        }

        private void BindRecipeTypes()
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            var types = from t in dine.DS_RecipeTypes
                        orderby t.Name
                        select t;
            ddlRecipeTypes.DataSource = types;
            ddlRecipeTypes.DataTextField = "name";
            ddlRecipeTypes.DataValueField = "recipetypeID";
            if (types.Count() > 1)
            {
                ddlRecipeTypes.Items.Add(new ListItem("", ""));
            }
            ddlRecipeTypes.DataBind();
        }

        private void BindDataRecipe(DS_Recipe recipe)
        {
            ddlRecipeTypes.SelectedValue = recipe.RecipeTypeID + "";
            txtName.Text = recipe.Name;
            txtAuthor.Text = recipe.Author;
            txtDirections.Text = recipe.Directions;
            txtIngredients.Text = recipe.Ingredients;
            txtLink.Text = recipe.SourceLink;
            txtNotes.Text = recipe.Notes;
            txtPreparation.Text = recipe.PreparationTimeMinutes.HasValue ? recipe.PreparationTimeMinutes.Value + "" : "";
            txtReady.Text = recipe.ReadyInTimeMinutes.HasValue ? recipe.ReadyInTimeMinutes.Value + "" : "";
            txtServings.Text = recipe.Servings.HasValue ? recipe.Servings.Value + "" : "";
            txtShortDescription.Text = recipe.ShortDescription;
            if (!recipe.Picture.Equals(""))
            {
                Literal lit = new Literal();
                lit.Text = "<a href=\"http://www.dineselect.com/" + recipe.Picture + 
                        "\"><img src=\"Thumbnail.ashx?p=" + recipe.Picture + "\"></a>";
                panelPic.Controls.Add(lit);
            }
        }

        private int getMaximumNumberFromFolder(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo[] rgFiles = di.GetFiles("*.*");
            List<int> numFiles = new List<int>();

            foreach (FileInfo fi in rgFiles)
            {
                string name = Path.GetFileNameWithoutExtension(fi.Name);
                name = name.Replace("Image", "");
                int n = int.Parse(name);
                numFiles.Add(n);
            }
            return (numFiles.Count() > 0 ? numFiles.Max() + 1 : 1);
        }


        protected void linkInsert_Click(object sender, EventArgs e)
        {
            DineSelectDataContext dine = new DineSelectDataContext();
            DS_Recipe est = new DS_Recipe();

            est.Author = txtAuthor.Text;
            est.Directions = txtDirections.Text;
            est.Ingredients = txtIngredients.Text;
            est.Name = txtName.Text;
            est.Notes = txtNotes.Text;
            //est.Picture =
            if (!txtPreparation.Text.Equals(""))
            {
                est.PreparationTimeMinutes = int.Parse(txtPreparation.Text);
            }
            if (!txtReady.Text.Equals(""))
            {
                est.ReadyInTimeMinutes = int.Parse(txtReady.Text);
            }
            est.RecipeTypeID = int.Parse(ddlRecipeTypes.SelectedValue);
            if (!txtServings.Text.Equals(""))
            {
                est.Servings = int.Parse(txtServings.Text);
            }
            est.ShortDescription = txtShortDescription.Text;
            est.SourceLink = txtLink.Text;

            if (fileUpload.HasFile)
            {
                string path = Server.MapPath("~/Pictures/");
                path = path + "//Recipes";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                int max = getMaximumNumberFromFolder(path);
                string ext = Path.GetExtension(fileUpload.FileName);
                string img_name = "Image" + max + ext;
                path = path + "//" + img_name;

                fileUpload.SaveAs(path);

                est.Picture = "Recipes/" + img_name;
            }
            dine.DS_Recipes.InsertOnSubmit(est);
            dine.SubmitChanges();
            Response.Redirect("Recipes.aspx");
        }

        protected void linkUpdate_Click(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            DineSelectDataContext dine = new DineSelectDataContext();
            var establ = from esta in dine.DS_Recipes
                         where esta.RecipeID == int.Parse(id)
                         select esta;
            DS_Recipe est = establ.First();

            est.Author = txtAuthor.Text;
            est.Directions = txtDirections.Text;
            est.Ingredients = txtIngredients.Text;
            est.Name = txtName.Text;
            est.Notes = txtNotes.Text;
            //est.Picture =
            if (!txtPreparation.Text.Equals(""))
            {
                est.PreparationTimeMinutes = int.Parse(txtPreparation.Text);
            }
            if (!txtReady.Text.Equals(""))
            {
                est.ReadyInTimeMinutes = int.Parse(txtReady.Text);
            }
            est.RecipeTypeID = int.Parse(ddlRecipeTypes.SelectedValue);
            if (!txtServings.Text.Equals(""))
            {
                est.Servings = int.Parse(txtServings.Text);
            }
            est.ShortDescription = txtShortDescription.Text;
            est.SourceLink = txtLink.Text;

            if (fileUpload.HasFile)
            {
                string path = Server.MapPath("~/Pictures/");
                if (est.Picture != null && !est.Picture.Equals(""))
                {
                    File.Delete(path + est.Picture);
                }
                path = path + "//Recipes";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                int max = getMaximumNumberFromFolder(path);
                string ext = Path.GetExtension(fileUpload.FileName);
                string img_name = "Image" + max + ext;
                path = path + "//" + img_name;

                fileUpload.SaveAs(path);

                est.Picture = "Recipes/" + img_name;
            }

            dine.SubmitChanges();
            LinqRecipes.DataBind();
            LinqRecipes.DataBind();
            UpdatePanel1.Update();
            Response.Redirect("ViewEditRecipe.aspx?id=" + id + "&mode=view");
        }

        protected void linkCancel_Click(object sender, EventArgs e)
        {
            string mode = Request.QueryString["mode"];
            string id = Request.QueryString["id"];
            if (mode != null && mode.Equals("edit"))
            {
                Response.Redirect("ViewEditRecipe.aspx?id=" + id + "&mode=view");
            }
            else
            {
                Response.Redirect("Recipes.aspx");
            }
        }

    }
}
